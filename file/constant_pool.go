package file

type ConstantPool struct {
	classFile     *ClassFile
	constantInfos []ConstantInfo
}

func (constantPool *ConstantPool) read(classReader *ClassReader) {
	count := int(classReader.readUint16())
	constantPool.constantInfos = make([]ConstantInfo, count)
	for i := 1; i < count; i++ {
		constantPool.constantInfos[i] = readConstantInfo(classReader, constantPool)
		switch constantPool.constantInfos[i].(type) {
		case *LongConstantInfo, *DoubleConstantInfo:
			i++
		}
	}
}

func (constantPool *ConstantPool) ConstantInfos() []ConstantInfo {
	return constantPool.constantInfos
}

func (constantPool *ConstantPool) getConstantInfo(index uint16) ConstantInfo {
	if constantInfo := constantPool.constantInfos[index]; constantInfo != nil {
		return constantInfo
	}
	panic("Invalid constant pool index")
}

func (constantPool *ConstantPool) getNameAndDescriptor(index uint16) (string, string) {
	constantInfo := constantPool.getConstantInfo(index)
	descriptorConstantInfo := constantInfo.(*DescriptorConstantInfo)
	name := constantPool.getUtf8(descriptorConstantInfo.nameIndex)
	descriptor := constantPool.getUtf8(descriptorConstantInfo.descriptorIndex)
	return name, descriptor
}

func (constantPool *ConstantPool) getClassName(index uint16) string {
	constantInfo := constantPool.getConstantInfo(index)
	classConstantInfo := constantInfo.(*ClassConstantInfo)
	return constantPool.getUtf8(classConstantInfo.nameIndex)
}

func (constantPool *ConstantPool) getUtf8(index uint16) string {
	constantInfo := constantPool.getConstantInfo(index)
	return constantInfo.(*Utf8ConstantInfo).value
}
