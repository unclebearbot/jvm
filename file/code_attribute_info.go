package file

type CodeAttributeInfo struct {
	constantPool           *ConstantPool
	threadStackCapacity    uint16
	localVariableTableSize uint16
	code                   []byte
	exceptions             []*Exception
	attributeInfos         []AttributeInfo
}

type Exception struct {
	start     uint16
	end       uint16
	handler   uint16
	catchType uint16
}

func (codeAttributeInfo *CodeAttributeInfo) read(classReader *ClassReader) {
	codeAttributeInfo.threadStackCapacity = classReader.readUint16()
	codeAttributeInfo.localVariableTableSize = classReader.readUint16()
	length := classReader.readUint32()
	codeAttributeInfo.code = classReader.readBytes(length)
	codeAttributeInfo.exceptions = readExceptions(classReader)
	codeAttributeInfo.attributeInfos = readAttributeInfos(classReader, codeAttributeInfo.constantPool)
}

func readExceptions(classReader *ClassReader) []*Exception {
	length := classReader.readUint16()
	exceptions := make([]*Exception, length)
	for i := range exceptions {
		exceptions[i] = &Exception{
			start:     classReader.readUint16(),
			end:       classReader.readUint16(),
			handler:   classReader.readUint16(),
			catchType: classReader.readUint16(),
		}
	}
	return exceptions
}

func (codeAttributeInfo *CodeAttributeInfo) ThreadStackCapacity() uint {
	return uint(codeAttributeInfo.threadStackCapacity)
}

func (codeAttributeInfo *CodeAttributeInfo) LocalVariableTableSize() uint {
	return uint(codeAttributeInfo.localVariableTableSize)
}

func (codeAttributeInfo *CodeAttributeInfo) Code() []byte {
	return codeAttributeInfo.code
}
