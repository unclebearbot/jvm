package file

import (
	"strconv"
)

type ClassFile struct {
	magic          uint32
	minorVersion   uint16
	majorVersion   uint16
	constantPool   *ConstantPool
	accessFlags    uint16
	thisClass      uint16
	superClass     uint16
	interfaces     []uint16
	fields         []*MemberInfo
	methods        []*MemberInfo
	attributeInfos []AttributeInfo
}

func Parse(classData []byte) *ClassFile {
	classReader := &ClassReader{classData}
	classFile := &ClassFile{}
	return classFile.parseClassFile(classReader)
}

func (classFile *ClassFile) parseClassFile(classReader *ClassReader) *ClassFile {
	classFile.readMagic(classReader)
	classFile.readVersion(classReader)
	classFile.readConstantPool(classReader)
	classFile.accessFlags = classReader.readUint16()
	classFile.thisClass = classReader.readUint16()
	classFile.superClass = classReader.readUint16()
	classFile.interfaces = classReader.readUint16s()
	classFile.fields = readMembers(classReader, classFile.constantPool)
	classFile.methods = readMembers(classReader, classFile.constantPool)
	classFile.attributeInfos = readAttributeInfos(classReader, classFile.constantPool)
	return classFile
}

func (classFile *ClassFile) readMagic(classReader *ClassReader) {
	if magic := classReader.readUint32(); magic != 0xcafebabe {
		panic("java.lang.ClassFormatError: Incompatible magic value 0x" + strconv.FormatInt(int64(magic), 16))
	}
}

func (classFile *ClassFile) readVersion(classReader *ClassReader) {
	classFile.minorVersion = classReader.readUint16()
	classFile.majorVersion = classReader.readUint16()
	if classFile.majorVersion < 45 || classFile.majorVersion > 52 {
		panic("java.lang.UnsupportedClassVersionError: " + strconv.Itoa(int(classFile.majorVersion)) + "." + strconv.Itoa(int(classFile.minorVersion)))
	}
}

func (classFile *ClassFile) readConstantPool(classReader *ClassReader) {
	classFile.constantPool = &ConstantPool{classFile: classFile}
	classFile.constantPool.read(classReader)
}

func (classFile *ClassFile) Magic() uint32 {
	return classFile.magic
}

func (classFile *ClassFile) MinorVersion() uint16 {
	return classFile.minorVersion
}

func (classFile *ClassFile) MajorVersion() uint16 {
	return classFile.majorVersion
}

func (classFile *ClassFile) ConstantPool() *ConstantPool {
	return classFile.constantPool
}

func (classFile *ClassFile) AccessFlags() uint16 {
	return classFile.accessFlags
}

func (classFile *ClassFile) ThisClass() uint16 {
	return classFile.thisClass
}

func (classFile *ClassFile) ClassName() string {
	return classFile.constantPool.getClassName(classFile.thisClass)
}

func (classFile *ClassFile) SuperClass() uint16 {
	return classFile.superClass
}

func (classFile *ClassFile) SuperClassName() string {
	if classFile.superClass != 0 {
		return classFile.constantPool.getClassName(classFile.superClass)
	}
	return ""
}

func (classFile *ClassFile) Interfaces() []uint16 {
	return classFile.interfaces
}

func (classFile *ClassFile) InterfaceNames() []string {
	interfaceNames := make([]string, len(classFile.interfaces))
	for i, index := range classFile.interfaces {
		interfaceNames[i] = classFile.constantPool.getClassName(index)
	}
	return interfaceNames
}

func (classFile *ClassFile) Fields() []*MemberInfo {
	return classFile.fields
}

func (classFile *ClassFile) Methods() []*MemberInfo {
	return classFile.methods
}

func (classFile *ClassFile) AttributeInfos() []AttributeInfo {
	return classFile.attributeInfos
}
