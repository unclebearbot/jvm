package file

type MethodTypeConstantInfo struct {
	descriptorIndex uint16
}

func (methodTypeConstantInfo *MethodTypeConstantInfo) read(classReader *ClassReader) {
	methodTypeConstantInfo.descriptorIndex = classReader.readUint16()
}

type MethodHandleConstantInfo struct {
	referenceKind  uint8
	referenceIndex uint16
}

func (methodHandleConstantInfo *MethodHandleConstantInfo) read(classReader *ClassReader) {
	methodHandleConstantInfo.referenceKind = classReader.readUint8()
	methodHandleConstantInfo.referenceIndex = classReader.readUint16()
}

func (methodHandleConstantInfo *MethodHandleConstantInfo) ReferenceKind() uint8 {
	return methodHandleConstantInfo.referenceKind
}

func (methodHandleConstantInfo *MethodHandleConstantInfo) ReferenceIndex() uint16 {
	return methodHandleConstantInfo.referenceIndex
}

type InvokeDynamicConstantInfo struct {
	constantPool                  *ConstantPool
	bootstrapMethodAttributeIndex uint16
	descriptorIndex               uint16
}

func (invokeDynamicConstantInfo *InvokeDynamicConstantInfo) read(classReader *ClassReader) {
	invokeDynamicConstantInfo.bootstrapMethodAttributeIndex = classReader.readUint16()
	invokeDynamicConstantInfo.descriptorIndex = classReader.readUint16()
}

func (invokeDynamicConstantInfo *InvokeDynamicConstantInfo) Descriptor() (string, string) {
	return invokeDynamicConstantInfo.constantPool.getNameAndDescriptor(invokeDynamicConstantInfo.descriptorIndex)
}

func (invokeDynamicConstantInfo *InvokeDynamicConstantInfo) BootstrapMethodAttribute() (uint16, []uint16) {
	// TODO
	return 0, nil
}
