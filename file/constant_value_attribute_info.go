package file

type ConstantValueAttributeInfo struct {
	constantValueIndex uint16
}

func (constantValueAttributeInfo *ConstantValueAttributeInfo) read(classReader *ClassReader) {
	constantValueAttributeInfo.constantValueIndex = classReader.readUint16()
}

func (constantValueAttributeInfo *ConstantValueAttributeInfo) ConstantValueIndex() uint16 {
	return constantValueAttributeInfo.constantValueIndex
}
