package file

type MemberInfo struct {
	constantPool    *ConstantPool
	accessFlags     uint16
	nameIndex       uint16
	descriptorIndex uint16
	attributeInfos  []AttributeInfo
}

func readMembers(classReader *ClassReader, constantPool *ConstantPool) []*MemberInfo {
	memberCount := classReader.readUint16()
	members := make([]*MemberInfo, memberCount)
	for i := range members {
		members[i] = readMember(classReader, constantPool)
	}
	return members
}

func readMember(classReader *ClassReader, constantPool *ConstantPool) *MemberInfo {
	accessFlags := classReader.readUint16()
	nameIndex := classReader.readUint16()
	descriptorIndex := classReader.readUint16()
	attributeInfos := readAttributeInfos(classReader, constantPool)
	return &MemberInfo{
		constantPool:    constantPool,
		accessFlags:     accessFlags,
		nameIndex:       nameIndex,
		descriptorIndex: descriptorIndex,
		attributeInfos:  attributeInfos,
	}
}

func (memberInfo *MemberInfo) AccessFlags() uint16 {
	return memberInfo.accessFlags
}

func (memberInfo *MemberInfo) Name() string {
	return memberInfo.constantPool.getUtf8(memberInfo.nameIndex)
}

func (memberInfo *MemberInfo) Descriptor() string {
	return memberInfo.constantPool.getUtf8(memberInfo.descriptorIndex)
}

func (memberInfo *MemberInfo) CodeAttributeInfo() *CodeAttributeInfo {
	for _, attributeInfo := range memberInfo.attributeInfos {
		switch attributeInfo.(type) {
		case *CodeAttributeInfo:
			return attributeInfo.(*CodeAttributeInfo)
		}
	}
	panic("Code attribute info not found")
}

func (memberInfo *MemberInfo) ConstantValueAttributeInfo() *ConstantValueAttributeInfo {
	for _, attributeInfo := range memberInfo.attributeInfos {
		switch attributeInfo.(type) {
		case *ConstantValueAttributeInfo:
			return attributeInfo.(*ConstantValueAttributeInfo)
		}
	}
	return nil
}
