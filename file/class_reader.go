package file

import (
	"encoding/binary"
)

type ClassReader struct {
	data []byte
}

func (classReader *ClassReader) readUint8() uint8 {
	read := classReader.data[0]
	classReader.data = classReader.data[1:]
	return read
}

func (classReader *ClassReader) readUint16() uint16 {
	read := binary.BigEndian.Uint16(classReader.data)
	classReader.data = classReader.data[2:]
	return read
}

func (classReader *ClassReader) readUint32() uint32 {
	read := binary.BigEndian.Uint32(classReader.data)
	classReader.data = classReader.data[4:]
	return read
}

func (classReader *ClassReader) readUint64() uint64 {
	read := binary.BigEndian.Uint64(classReader.data)
	classReader.data = classReader.data[8:]
	return read
}

func (classReader *ClassReader) readUint16s() []uint16 {
	size := classReader.readUint16()
	read := make([]uint16, size)
	for i := range read {
		read[i] = classReader.readUint16()
	}
	return read
}

func (classReader *ClassReader) readBytes(size uint32) []byte {
	read := classReader.data[:size]
	classReader.data = classReader.data[size:]
	return read
}
