package file

const (
	utf8ConstantInfo            = 1
	integerConstantInfo         = 3
	floatConstantInfo           = 4
	longConstantInfo            = 5
	doubleConstantInfo          = 6
	classConstantInfo           = 7
	stringConstantInfo          = 8
	fieldConstantInfo           = 9
	methodConstantInfo          = 10
	interfaceMethodConstantInfo = 11
	descriptorConstantInfo      = 12
	methodHandleConstantInfo    = 15
	methodTypeConstantInfo      = 16
	invokeDynamicConstantInfo   = 18
)

type ConstantInfo interface {
	read(classReader *ClassReader)
}

func readConstantInfo(classReader *ClassReader, constantPool *ConstantPool) ConstantInfo {
	constantPoolTag := classReader.readUint8()
	constantInfo := newConstantInfo(constantPoolTag, constantPool)
	constantInfo.read(classReader)
	return constantInfo
}

func newConstantInfo(constantPoolTag uint8, constantPool *ConstantPool) ConstantInfo {
	switch constantPoolTag {
	case integerConstantInfo:
		return &IntegerConstantInfo{}
	case floatConstantInfo:
		return &FloatConstantInfo{}
	case longConstantInfo:
		return &LongConstantInfo{}
	case doubleConstantInfo:
		return &DoubleConstantInfo{}
	case utf8ConstantInfo:
		return &Utf8ConstantInfo{}
	case stringConstantInfo:
		return &StringConstantInfo{constantPool: constantPool}
	case classConstantInfo:
		return &ClassConstantInfo{constantPool: constantPool}
	case fieldConstantInfo:
		return &FieldConstantInfo{MemberConstantInfo{constantPool: constantPool}}
	case methodConstantInfo:
		return &MethodConstantInfo{MemberConstantInfo{constantPool: constantPool}}
	case interfaceMethodConstantInfo:
		return &InterfaceMethodConstantInfo{MemberConstantInfo{constantPool: constantPool}}
	case descriptorConstantInfo:
		return &DescriptorConstantInfo{}
	case methodTypeConstantInfo:
		return &MethodTypeConstantInfo{}
	case methodHandleConstantInfo:
		return &MethodHandleConstantInfo{}
	case invokeDynamicConstantInfo:
		return &InvokeDynamicConstantInfo{constantPool: constantPool}
	default:
		panic("java.lang.ClassFormatError: constant pool tag")
	}
}
