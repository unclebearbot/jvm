package file

type StringConstantInfo struct {
	constantPool *ConstantPool
	stringIndex  uint16
}

func (stringConstantInfo *StringConstantInfo) read(classReader *ClassReader) {
	stringConstantInfo.stringIndex = classReader.readUint16()
}

func (stringConstantInfo *StringConstantInfo) String() string {
	return stringConstantInfo.constantPool.getUtf8(stringConstantInfo.stringIndex)
}
