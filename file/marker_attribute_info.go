package file

type MarkerAttributeInfo struct{}

func (markerAttributeInfo *MarkerAttributeInfo) read(classReader *ClassReader) {}

type DeprecatedAttributeInfo struct{ MarkerAttributeInfo }

type SyntheticAttributeInfo struct{ MarkerAttributeInfo }
