package file

type LineNumbersAttributeInfo struct {
	lineNumbers []*LineNumber
}

type LineNumber struct {
	start      uint16
	lineNumber uint16
}

func (lineNumbersAttributeInfo *LineNumbersAttributeInfo) read(classReader *ClassReader) {
	length := classReader.readUint16()
	lineNumbersAttributeInfo.lineNumbers = make([]*LineNumber, length)
	for i := range lineNumbersAttributeInfo.lineNumbers {
		lineNumbersAttributeInfo.lineNumbers[i] = &LineNumber{
			start:      classReader.readUint16(),
			lineNumber: classReader.readUint16(),
		}
	}
}
