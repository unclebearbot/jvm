package file

type LocalVariablesAttributeInfo struct {
	localVariables []*LocalVariable
}

type LocalVariable struct {
	start          uint16
	length         uint16
	nameIndex      uint16
	signatureIndex uint16
	index          uint16
}

func (localVariablesAttributeInfo *LocalVariablesAttributeInfo) read(classReader *ClassReader) {
	length := classReader.readUint16()
	localVariablesAttributeInfo.localVariables = make([]*LocalVariable, length)
	for i := range localVariablesAttributeInfo.localVariables {
		localVariablesAttributeInfo.localVariables[i] = &LocalVariable{
			start:          classReader.readUint16(),
			length:         classReader.readUint16(),
			nameIndex:      classReader.readUint16(),
			signatureIndex: classReader.readUint16(),
			index:          classReader.readUint16(),
		}
	}
}
