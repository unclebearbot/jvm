package file

type DescriptorConstantInfo struct {
	nameIndex       uint16
	descriptorIndex uint16
}

func (descriptorConstantInfo *DescriptorConstantInfo) read(classReader *ClassReader) {
	descriptorConstantInfo.nameIndex = classReader.readUint16()
	descriptorConstantInfo.descriptorIndex = classReader.readUint16()
}
