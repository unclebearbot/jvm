package file

type SourceFileAttributeInfo struct {
	constantPool    *ConstantPool
	sourceFileIndex uint16
}

func (sourceFileAttributeInfo *SourceFileAttributeInfo) read(classReader *ClassReader) {
	sourceFileAttributeInfo.sourceFileIndex = classReader.readUint16()
}

func (sourceFileAttributeInfo *SourceFileAttributeInfo) FileName() string {
	return sourceFileAttributeInfo.constantPool.getUtf8(sourceFileAttributeInfo.sourceFileIndex)
}
