package file

type ExceptionsAttributeInfo struct {
	exceptionsIndex []uint16
}

func (exceptionsAttributeInfo *ExceptionsAttributeInfo) read(classReader *ClassReader) {
	exceptionsAttributeInfo.exceptionsIndex = classReader.readUint16s()
}

func (exceptionsAttributeInfo *ExceptionsAttributeInfo) ExceptionsIndex() []uint16 {
	return exceptionsAttributeInfo.exceptionsIndex
}
