package file

type AttributeInfo interface {
	read(classReader *ClassReader)
}

func readAttributeInfos(classReader *ClassReader, constantPool *ConstantPool) []AttributeInfo {
	count := classReader.readUint16()
	attributeInfos := make([]AttributeInfo, count)
	for i := range attributeInfos {
		attributeInfo := readAttributeInfo(classReader, constantPool)
		attributeInfos[i] = attributeInfo
	}
	return attributeInfos
}

func readAttributeInfo(classReader *ClassReader, constantPool *ConstantPool) AttributeInfo {
	nameIndex := classReader.readUint16()
	length := classReader.readUint32()
	name := constantPool.getUtf8(nameIndex)
	attributeInfo := newAttributeInfo(name, length, constantPool)
	attributeInfo.read(classReader)
	return attributeInfo
}

func newAttributeInfo(name string, length uint32, constantPool *ConstantPool) AttributeInfo {
	switch name {
	case "Code":
		return &CodeAttributeInfo{constantPool: constantPool}
	case "ConstantValue":
		return &ConstantValueAttributeInfo{}
	case "Deprecated":
		return &DeprecatedAttributeInfo{}
	case "Exceptions":
		return &ExceptionsAttributeInfo{}
	case "LineNumberTable":
		return &LineNumbersAttributeInfo{}
	case "LocalVariableTable":
		return &LocalVariablesAttributeInfo{}
	case "SourceFile":
		return &SourceFileAttributeInfo{constantPool: constantPool}
	case "Synthetic":
		return &SyntheticAttributeInfo{}
	default:
		return &UnparsedAttributeInfo{name: name, length: length}
	}
}
