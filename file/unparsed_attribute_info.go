package file

type UnparsedAttributeInfo struct {
	name   string
	length uint32
	info   []byte
}

func (unparsedAttributeInfo *UnparsedAttributeInfo) read(classReader *ClassReader) {
	unparsedAttributeInfo.info = classReader.readBytes(unparsedAttributeInfo.length)
}

func (unparsedAttributeInfo *UnparsedAttributeInfo) Info() []byte {
	return unparsedAttributeInfo.info
}
