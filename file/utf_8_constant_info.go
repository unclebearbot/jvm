package file

import (
	"strconv"
	"unicode/utf16"
)

type Utf8ConstantInfo struct {
	value string
}

func (utf8ConstantInfo *Utf8ConstantInfo) read(classReader *ClassReader) {
	length := classReader.readUint16()
	bytes := classReader.readBytes(uint32(length))
	utf8ConstantInfo.value = stringFromBytesMutf8(bytes)
}

func stringFromBytesMutf8(bytes []byte) string {
	length := len(bytes)
	chars := make([]uint16, length)
	var char1, char2, char3 uint16
	count := 0
	index := 0
	for count < length {
		char1 = uint16(bytes[count])
		if char1 > 127 {
			break
		}
		count++
		chars[index] = char1
		index++
	}
	for count < length {
		char1 = uint16(bytes[count])
		switch char1 >> 4 {
		case 0, 1, 2, 3, 4, 5, 6, 7:
			count++
			chars[index] = char1
			index++
		case 12, 13:
			count += 2
			if count > length {
				panic("Malformed string: unexpected EOF")
			}
			char2 = uint16(bytes[count-1])
			if char2&0xC0 != 0x80 {
				panic("Malformed string: at byte " + strconv.Itoa(count))
			}
			chars[index] = char1&0x1F<<6 | char2&0x3F
			index++
		case 14:
			count += 3
			if count > length {
				panic("Malformed string: unexpected EOF")
			}
			char2 = uint16(bytes[count-2])
			char3 = uint16(bytes[count-1])
			if char2&0xC0 != 0x80 || char3&0xC0 != 0x80 {
				panic("Malformed string: at byte " + strconv.Itoa(count-1))
			}
			chars[index] = char1&0x0F<<12 | char2&0x3F<<6 | char3&0x3F<<0
			index++
		default:
			panic("Malformed string: at byte " + strconv.Itoa(count))
		}
	}
	return string(utf16.Decode(chars[0:index]))
}
