package file

import "math"

type IntegerConstantInfo struct {
	value int32
}

func (integerConstantInfo *IntegerConstantInfo) read(classReader *ClassReader) {
	integerConstantInfo.value = int32(classReader.readUint32())
}

func (integerConstantInfo *IntegerConstantInfo) Value() int32 {
	return integerConstantInfo.value
}

type FloatConstantInfo struct {
	value float32
}

func (floatConstantInfo *FloatConstantInfo) read(classReader *ClassReader) {
	floatConstantInfo.value = math.Float32frombits(classReader.readUint32())
}

func (floatConstantInfo *FloatConstantInfo) Value() float32 {
	return floatConstantInfo.value
}

type LongConstantInfo struct {
	value int64
}

func (longConstantInfo *LongConstantInfo) read(classReader *ClassReader) {
	longConstantInfo.value = int64(classReader.readUint64())
}

func (longConstantInfo *LongConstantInfo) Value() int64 {
	return longConstantInfo.value
}

type DoubleConstantInfo struct {
	value float64
}

func (doubleConstantInfo *DoubleConstantInfo) read(classReader *ClassReader) {
	doubleConstantInfo.value = math.Float64frombits(classReader.readUint64())
}

func (doubleConstantInfo *DoubleConstantInfo) Value() float64 {
	return doubleConstantInfo.value
}
