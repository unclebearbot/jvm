package file

type MemberConstantInfo struct {
	constantPool    *ConstantPool
	classIndex      uint16
	descriptorIndex uint16
}

func (memberConstantInfo *MemberConstantInfo) read(classReader *ClassReader) {
	memberConstantInfo.classIndex = classReader.readUint16()
	memberConstantInfo.descriptorIndex = classReader.readUint16()
}

func (memberConstantInfo *MemberConstantInfo) ClassName() string {
	return memberConstantInfo.constantPool.getClassName(memberConstantInfo.classIndex)
}

func (memberConstantInfo *MemberConstantInfo) NameAndDescriptor() (string, string) {
	return memberConstantInfo.constantPool.getNameAndDescriptor(memberConstantInfo.descriptorIndex)
}

type FieldConstantInfo struct{ MemberConstantInfo }

type MethodConstantInfo struct{ MemberConstantInfo }

type InterfaceMethodConstantInfo struct{ MemberConstantInfo }
