package file

type ClassConstantInfo struct {
	constantPool *ConstantPool
	nameIndex    uint16
}

func (classConstantInfo *ClassConstantInfo) read(classReader *ClassReader) {
	classConstantInfo.nameIndex = classReader.readUint16()
}

func (classConstantInfo *ClassConstantInfo) Name() string {
	return classConstantInfo.constantPool.getUtf8(classConstantInfo.nameIndex)
}
