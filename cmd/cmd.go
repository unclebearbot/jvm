package cmd

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/nvxarm/jvm/classpath"
	"bitbucket.org/nvxarm/jvm/file"
	"bitbucket.org/nvxarm/jvm/interpreter"
)

type Cmd struct {
	help      bool
	version   bool
	jre       string
	classpath string
	class     string
	args      []string
}

func Start() {
	cmd := newCmd()
	if cmd.version {
		version()
		return
	}
	if cmd.help || cmd.class == "" {
		help()
		return
	}
	path := classpath.Parse(cmd.jre, cmd.classpath)
	className := strings.Replace(cmd.class, ".", "/", -1)
	classFile := loadClass(className, path)
	invokeMainMethod(classFile)
}

func newCmd() *Cmd {
	cmd := &Cmd{}
	flag.Usage = help
	flag.BoolVar(&cmd.help, "help", false, "print help")
	flag.BoolVar(&cmd.version, "version", false, "print version")
	flag.StringVar(&cmd.jre, "Xjre", "", "path to JRE")
	flag.StringVar(&cmd.classpath, "classpath", "", "classpath")
	flag.StringVar(&cmd.classpath, "cp", "", "classpath")
	flag.Parse()
	args := flag.Args()
	if len(args) > 0 {
		cmd.class = args[0]
		cmd.args = args[1:]
	}
	return cmd
}

func version() {
	fmt.Fprintln(os.Stderr, "Version: 1.0")
}

func help() {
	fmt.Fprintln(os.Stderr, "Usage:")
	fmt.Fprintln(os.Stderr, "\t"+os.Args[0]+" [option...] <class> [argument...]")
	fmt.Fprintln(os.Stderr)
	fmt.Fprintln(os.Stderr, "Options:")
	fmt.Fprintln(os.Stderr, "\t-cp | -classpath <path>")
	fmt.Fprintln(os.Stderr, "\t-version")
	fmt.Fprintln(os.Stderr, "\t-help")
	fmt.Fprintln(os.Stderr)
	fmt.Fprintln(os.Stderr, "Non-standard options:")
	fmt.Fprintln(os.Stderr, "\t-Xjre <path>")
}

func loadClass(className string, classpath *classpath.Classpath) *file.ClassFile {
	classData, _ := classpath.ReadClass(className)
	classFile := file.Parse(classData)
	return classFile
}

func invokeMainMethod(classFile *file.ClassFile) {
	mainMethod := getMainMethod(classFile)
	interpreter.Interpret(mainMethod)
}

func getMainMethod(classFile *file.ClassFile) *file.MemberInfo {
	for _, memberInfo := range classFile.Methods() {
		if memberInfo.Name() == "main" && memberInfo.Descriptor() == "([Ljava/lang/String;)V" {
			return memberInfo
		}
	}
	panic("Main method not found in class " + classFile.ClassName())
}
