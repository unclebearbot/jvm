package classpath

import (
	"errors"
	"io/ioutil"
	"path/filepath"
)

type DirClasspathEntry struct {
	absPath string
}

func newDirClasspathEntry(path string) *DirClasspathEntry {
	absPath, e := filepath.Abs(path)
	if e != nil {
		panic(e)
	}
	return &DirClasspathEntry{absPath}
}

func (dirClasspathEntry *DirClasspathEntry) readClass(className string) ([]byte, ClasspathEntry, error) {
	file := filepath.Join(dirClasspathEntry.absPath, className)
	data, e := ioutil.ReadFile(file)
	if e != nil {
		return nil, nil, errors.New("Class not found: " + className)
	}
	return data, dirClasspathEntry, nil
}

func (dirClasspathEntry *DirClasspathEntry) String() string {
	return dirClasspathEntry.absPath
}
