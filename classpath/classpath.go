package classpath

import (
	"os"
	"path/filepath"
)

type Classpath struct {
	boot ClasspathEntry
	ext  ClasspathEntry
	user ClasspathEntry
}

func Parse(jre string, path string) *Classpath {
	classpath := &Classpath{}
	classpath.parseSystemClasspath(jre)
	classpath.parseUserClasspath(path)
	return classpath
}

func (classpath *Classpath) parseSystemClasspath(jre string) {
	jreDir := jreDir(jre)
	jreLibPath := filepath.Join(jreDir, "lib", "*")
	classpath.boot = newWildcardClasspathEntry(jreLibPath)
	jreExtPath := filepath.Join(jreDir, "lib", "ext", "*")
	classpath.ext = newWildcardClasspathEntry(jreExtPath)
}

func jreDir(jre string) string {
	if jre != "" && exist(jre) {
		return jre
	}
	if exist("jre") {
		return "jre"
	}
	if javaHome := os.Getenv("JAVA_HOME"); javaHome != "" {
		return filepath.Join(javaHome, "jre")
	}
	panic("JRE not found")
}

func exist(path string) bool {
	if _, e := os.Stat(path); e != nil {
		if os.IsNotExist(e) {
			return false
		}
	}
	return true
}

func (classpath *Classpath) parseUserClasspath(path string) {
	if path == "" {
		user := newClasspathEntry(".")
		classpath.user = user
	} else {
		user := newClasspathEntry(path)
		classpath.user = user
	}
}

func (classpath *Classpath) ReadClass(name string) ([]byte, ClasspathEntry) {
	class := name + ".class"
	if classData, classpathEntry, e := classpath.boot.readClass(class); e == nil {
		return classData, classpathEntry
	}
	if classData, classpathEntry, e := classpath.ext.readClass(class); e == nil {
		return classData, classpathEntry
	}
	if classData, classpathEntry, e := classpath.user.readClass(class); e == nil {
		return classData, classpathEntry
	}
	panic("Class not found: " + name)
}
