package classpath

import (
	"archive/zip"
	"errors"
	"io/ioutil"
	"path/filepath"
)

type ZipClasspathEntry struct {
	absPath string
}

func newZipClasspathEntry(path string) *ZipClasspathEntry {
	absPath, e := filepath.Abs(path)
	if e != nil {
		panic(e)
	}
	return &ZipClasspathEntry{absPath}
}

func (zipClasspathEntry *ZipClasspathEntry) readClass(className string) ([]byte, ClasspathEntry, error) {
	readCloser, e := zip.OpenReader(zipClasspathEntry.absPath)
	if e != nil {
		panic(e)
	}
	defer readCloser.Close()
	for _, file := range readCloser.File {
		if file.Name == className {
			reader, e := file.Open()
			if e != nil {
				panic(e)
			}
			defer reader.Close()
			data, e := ioutil.ReadAll(reader)
			if e != nil {
				panic(e)
			}
			return data, zipClasspathEntry, nil
		}
	}
	return nil, nil, errors.New("Class not found: " + className)
}

func (zipClasspathEntry *ZipClasspathEntry) String() string {
	return zipClasspathEntry.absPath
}
