package classpath

import (
	"errors"
	"strings"
)

type CompositeClasspathEntry []ClasspathEntry

func newCompositeClasspathEntry(pathList string) CompositeClasspathEntry {
	compositeClasspathEntry := []ClasspathEntry{}
	for _, path := range strings.Split(pathList, sep) {
		classpathEntry := newClasspathEntry(path)
		compositeClasspathEntry = append(compositeClasspathEntry, classpathEntry)
	}
	return compositeClasspathEntry
}

func (compositeClasspathEntry CompositeClasspathEntry) readClass(className string) ([]byte, ClasspathEntry, error) {
	for _, classpathEntry := range compositeClasspathEntry {
		data, from, e := classpathEntry.readClass(className)
		if e == nil {
			return data, from, nil
		}
	}
	return nil, nil, errors.New("Class not found: " + className)
}

func (compositeClasspathEntry CompositeClasspathEntry) String() string {
	absPaths := make([]string, len(compositeClasspathEntry))
	for i, classpathEntry := range compositeClasspathEntry {
		absPaths[i] = classpathEntry.String()
	}
	return strings.Join(absPaths, sep)
}
