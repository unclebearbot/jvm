package classpath

import (
	"os"
	"strings"
)

const sep = string(os.PathListSeparator)

type ClasspathEntry interface {
	readClass(className string) ([]byte, ClasspathEntry, error)
	String() string
}

func newClasspathEntry(path string) ClasspathEntry {
	if strings.Contains(path, sep) {
		return newCompositeClasspathEntry(path)
	}
	if strings.HasSuffix(path, "*") {
		return newWildcardClasspathEntry(path)
	}
	if strings.HasSuffix(path, ".jar") || strings.HasSuffix(path, ".JAR") || strings.HasSuffix(path, ".zip") || strings.HasSuffix(path, ".ZIP") {
		return newZipClasspathEntry(path)
	}
	return newDirClasspathEntry(path)
}
