package classpath

import (
	"os"
	"path/filepath"
	"strings"
)

func newWildcardClasspathEntry(path string) CompositeClasspathEntry {
	baseDir := path[:len(path)-1]
	compositeClasspathEntry := []ClasspathEntry{}
	filepath.Walk(baseDir, func(path string, info os.FileInfo, e error) error {
		if e != nil {
			return e
		}
		if info.IsDir() && path != baseDir {
			return filepath.SkipDir
		}
		if strings.HasSuffix(path, ".jar") || strings.HasSuffix(path, ".JAR") {
			zipClasspathEntry := newZipClasspathEntry(path)
			compositeClasspathEntry = append(compositeClasspathEntry, zipClasspathEntry)
		}
		return nil
	})
	return compositeClasspathEntry
}
