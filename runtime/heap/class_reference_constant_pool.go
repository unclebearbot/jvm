package heap

import (
	"bitbucket.org/nvxarm/jvm/file"
)

type ClassReferenceConstantPool struct {
	SymbolConstantPool
}

func newClassReferenceConstantPool(constantPool *ConstantPool, classConstantInfo *file.ClassConstantInfo) *ClassReferenceConstantPool {
	classReferenceConstantPool := &ClassReferenceConstantPool{}
	classReferenceConstantPool.constantPool = constantPool
	classReferenceConstantPool.className = classConstantInfo.Name()
	return classReferenceConstantPool
}
