package heap

import "bitbucket.org/nvxarm/jvm/file"

type Field struct {
	ClassMember
	constantValueIndex uint
	localVariableId    uint
}

func newFields(class *Class, classFileFleids []*file.MemberInfo) []*Field {
	fields := make([]*Field, len(classFileFleids))
	for i, field := range classFileFleids {
		fields[i] = &Field{}
		fields[i].class = class
		fields[i].copyMemberInfo(field)
		fields[i].copyAttributes(field)
	}
	return fields
}

func (field *Field) copyAttributes(memberInfo *file.MemberInfo) {
	if attribute := memberInfo.ConstantValueAttributeInfo(); attribute != nil {
		field.constantValueIndex = uint(attribute.ConstantValueIndex())
	}
}

func (field *Field) isLong() bool {
	return field.descriptor == "J"
}

func (field *Field) isDouble() bool {
	return field.descriptor == "D"
}

func (field *Field) LocalVariableId() uint {
	return field.localVariableId
}
