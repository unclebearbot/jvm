package heap

const (
	publicFlag       = 0x0001
	privateFlag      = 0x0002
	protectedFlag    = 0x0004
	staticFlag       = 0x0008
	finalFlag        = 0x0010
	superFlag        = 0x0020
	synchronizedFlag = 0x0020
	volatileFlag     = 0x0040
	bridgeFlag       = 0x0040
	transientFlag    = 0x0080
	varargsFlag      = 0x0080
	nativeFlag       = 0x0100
	interfaceFlag    = 0x0200
	abstractFlag     = 0x0400
	strictFlag       = 0x0800
	syntheticFlag    = 0x1000
	annotationFlag   = 0x2000
	enumFlag         = 0x4000
)

type AccessFlags struct {
	accessFlags uint16
}

func (accessFlags *AccessFlags) GetAccessFlags() uint16 {
	return accessFlags.accessFlags
}

func (accessFlags *AccessFlags) IsPublic() bool {
	return accessFlags.accessFlags&publicFlag != 0
}

func (accessFlags *AccessFlags) IsPrivate() bool {
	return accessFlags.accessFlags&privateFlag != 0
}

func (accessFlags *AccessFlags) IsProtected() bool {
	return accessFlags.accessFlags&protectedFlag != 0
}

func (accessFlags *AccessFlags) IsStatic() bool {
	return accessFlags.accessFlags&staticFlag != 0
}

func (accessFlags *AccessFlags) IsFinal() bool {
	return accessFlags.accessFlags&finalFlag != 0
}

func (accessFlags *AccessFlags) IsSuper() bool {
	return accessFlags.accessFlags&superFlag != 0
}

func (accessFlags *AccessFlags) IsSynchronized() bool {
	return accessFlags.accessFlags&synchronizedFlag != 0
}

func (accessFlags *AccessFlags) IsVolatile() bool {
	return accessFlags.accessFlags&volatileFlag != 0
}

func (accessFlags *AccessFlags) IsBridge() bool {
	return accessFlags.accessFlags&bridgeFlag != 0
}

func (accessFlags *AccessFlags) IsTransient() bool {
	return accessFlags.accessFlags&transientFlag != 0
}

func (accessFlags *AccessFlags) IsVarargs() bool {
	return accessFlags.accessFlags&varargsFlag != 0
}

func (accessFlags *AccessFlags) IsNative() bool {
	return accessFlags.accessFlags&nativeFlag != 0
}

func (accessFlags *AccessFlags) IsInterface() bool {
	return accessFlags.accessFlags&interfaceFlag != 0
}

func (accessFlags *AccessFlags) IsAbstract() bool {
	return accessFlags.accessFlags&abstractFlag != 0
}

func (accessFlags *AccessFlags) IsStrict() bool {
	return accessFlags.accessFlags&strictFlag != 0
}

func (accessFlags *AccessFlags) IsSynthetic() bool {
	return accessFlags.accessFlags&syntheticFlag != 0
}

func (accessFlags *AccessFlags) IsAnnotation() bool {
	return accessFlags.accessFlags&annotationFlag != 0
}

func (accessFlags *AccessFlags) IsEnum() bool {
	return accessFlags.accessFlags&enumFlag != 0
}
