package heap

import (
	"strconv"

	"bitbucket.org/nvxarm/jvm/file"
)

type ConstantPool struct {
	class     *Class
	constants []Constant
}

type Constant interface{}

func newConstantPool(class *Class, classFileConstantPool *file.ConstantPool) *ConstantPool {
	constantInfos := classFileConstantPool.ConstantInfos()
	size := len(constantInfos)
	constants := make([]Constant, size)
	constantPool := &ConstantPool{class, constants}
	for i := 1; i < size; i++ {
		constantInfo := constantInfos[i]
		switch constantInfo.(type) {
		case *file.IntegerConstantInfo:
			constants[i] = constantInfo.(*file.IntegerConstantInfo).Value()
		case *file.FloatConstantInfo:
			constants[i] = constantInfo.(*file.FloatConstantInfo).Value()
		case *file.LongConstantInfo:
			constants[i] = constantInfo.(*file.LongConstantInfo).Value()
			i++
		case *file.DoubleConstantInfo:
			constants[i] = constantInfo.(*file.DoubleConstantInfo).Value()
			i++
		case *file.StringConstantInfo:
			constants[i] = constantInfo.(*file.StringConstantInfo).String()
			i++
			// TODO
		}
	}
	return constantPool
}

func (constantPool *ConstantPool) GetConstant(index uint) Constant {
	if constant := constantPool.constants[index]; constant != nil {
		return constant
	}
	panic("Constant not found at index " + strconv.Itoa(int(index)))
}
