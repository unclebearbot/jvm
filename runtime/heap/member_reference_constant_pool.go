package heap

import "bitbucket.org/nvxarm/jvm/file"

type MemberReferenceConstantPool struct {
	SymbolConstantPool
	name       string
	descriptor string
}

func (memberReferenceConstantPool *MemberReferenceConstantPool) copyMemberConstantInfo(memberConstantInfo *file.MemberConstantInfo) {
	memberReferenceConstantPool.className = memberConstantInfo.ClassName()
	memberReferenceConstantPool.name, memberReferenceConstantPool.descriptor = memberConstantInfo.NameAndDescriptor()
}
