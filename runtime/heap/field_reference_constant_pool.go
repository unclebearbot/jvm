package heap

import "bitbucket.org/nvxarm/jvm/file"

type FieldReferenceConstantPool struct {
	MemberReferenceConstantPool
	field *Field
}

func newFieldReferenceConstantPool(constantPool *ConstantPool, fieldConstantInfo *file.FieldConstantInfo) *FieldReferenceConstantPool {
	fieldReferenceConstantPool := &FieldReferenceConstantPool{}
	fieldReferenceConstantPool.constantPool = constantPool
	fieldReferenceConstantPool.copyMemberConstantInfo(&fieldConstantInfo.MemberConstantInfo)
	return fieldReferenceConstantPool
}

func (fieldReferenceConstantPool *FieldReferenceConstantPool) Field() *Field {
	if fieldReferenceConstantPool.field == nil {
		fieldReferenceConstantPool.resolveField()
	}
	return fieldReferenceConstantPool.field
}

func (fieldReferenceConstantPool *FieldReferenceConstantPool) resolveField() {
	classSymbol := fieldReferenceConstantPool.constantPool.class
	class := fieldReferenceConstantPool.Class()
	field := lookUpField(class, fieldReferenceConstantPool.name, fieldReferenceConstantPool.descriptor)
	if field == nil {
		panic("java.lang.NoSuchFieldError")
	}
	if !field.isAccessibleTo(classSymbol) {
		panic("java.lang.IllegalAccessError")
	}
	fieldReferenceConstantPool.field = field
}

func lookUpField(class *Class, name string, descriptor string) *Field {
	for _, field := range class.fields {
		if field.name == name && field.descriptor == descriptor {
			return field
		}
	}
	for _, classInterface := range class.interfaces {
		if field := lookUpField(classInterface, name, descriptor); field != nil {
			return field
		}
	}
	if class.superClass != nil {
		return lookUpField(class.superClass, name, descriptor)
	}
	return nil
}
