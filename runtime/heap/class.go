package heap

import (
	"strings"

	"bitbucket.org/nvxarm/jvm/file"
)

type Class struct {
	AccessFlags
	name                         string
	superClassName               string
	interfaceNames               []string
	constantPool                 *ConstantPool
	fields                       []*Field
	methods                      []*Method
	classLoader                  *ClassLoader
	superClass                   *Class
	interfaces                   []*Class
	localVariableTableSize       uint
	localVariableTable           LocalVariableTable
	staticLocalVariableTableSize uint
	staticLocalVariableTable     LocalVariableTable
}

func newClass(classFile *file.ClassFile) *Class {
	class := &Class{}
	class.accessFlags = classFile.AccessFlags()
	class.name = classFile.ClassName()
	class.superClassName = classFile.SuperClassName()
	class.interfaceNames = classFile.InterfaceNames()
	class.constantPool = newConstantPool(class, classFile.ConstantPool())
	class.fields = newFields(class, classFile.Fields())
	class.methods = newMethods(class, classFile.Methods())
	return class
}

func (class *Class) isAccessibleTo(target *Class) bool {
	return class.IsPublic() || class.getPackageName() == target.getPackageName()
}

func (class *Class) getPackageName() string {
	if i := strings.LastIndex(class.name, "/"); i >= 0 {
		return class.name[:i]
	}
	return ""
}

func (class *Class) isSubClassOf(target *Class) bool {
	superClass := class.superClass
	if superClass == nil {
		return false
	}
	if superClass == class {
		return true
	}
	return superClass.isSubClassOf(target)
}

func (class *Class) NewObject() *Object {
	return newObject(class)
}

func (class *Class) ConstantPool() *ConstantPool {
	return class.constantPool
}

func (class *Class) LocalVariableTable() LocalVariableTable {
	return class.localVariableTable
}

func (class *Class) StaticLocalVariableTable() LocalVariableTable {
	return class.staticLocalVariableTable
}
