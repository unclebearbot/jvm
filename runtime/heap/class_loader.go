package heap

import (
	"bitbucket.org/nvxarm/jvm/classpath"
	"bitbucket.org/nvxarm/jvm/file"
)

type ClassLoader struct {
	classpath     *classpath.Classpath
	loadedClasses map[string]*Class
}

func NewClassLoader(classpath *classpath.Classpath) *ClassLoader {
	return &ClassLoader{
		classpath:     classpath,
		loadedClasses: make(map[string]*Class)}
}

func (classLoader *ClassLoader) LoadClass(name string) *Class {
	if class, loaded := classLoader.loadedClasses[name]; loaded {
		return class
	}
	return classLoader.loadClass(name)
}

func (classLoader *ClassLoader) loadClass(name string) *Class {
	data, classpathEntry := classLoader.readClass(name)
	class := classLoader.defineClass(data)
	verify(class)
	prepare(class)
	println("Loaded " + name + " from " + classpathEntry.String())
	return class
}

func (classLoader *ClassLoader) readClass(name string) ([]byte, classpath.ClasspathEntry) {
	return classLoader.classpath.ReadClass(name)
}

func (classLoader *ClassLoader) defineClass(data []byte) *Class {
	class := parseClass(data)
	class.classLoader = classLoader
	resolveSuperClass(class)
	resolveInterfaces(class)
	classLoader.loadedClasses[class.name] = class
	return class
}

func parseClass(data []byte) *Class {
	classFile := file.Parse(data)
	return newClass(classFile)
}

func resolveSuperClass(class *Class) {
	if class.name != "java/lang/Object" {
		class.superClass = class.classLoader.LoadClass(class.superClassName)
	}
}

func resolveInterfaces(class *Class) {
	count := len(class.interfaceNames)
	if count > 0 {
		class.interfaces = make([]*Class, count)
		for i, interfaceName := range class.interfaceNames {
			class.interfaces[i] = class.classLoader.LoadClass(interfaceName)
		}
	}
}

func verify(class *Class) {
	// TODO
}

func prepare(class *Class) {
	calculateInstanceFieldLocalVariableIds(class)
	calculateStaticFieldLocalVariableIds(class)
	allocateAndInitializeStaticLocalVariables(class)
}

func calculateInstanceFieldLocalVariableIds(class *Class) {
	localVariableId := uint(0)
	if class.superClass != nil {
		localVariableId = class.superClass.localVariableTableSize
	}
	for _, field := range class.fields {
		if !field.IsStatic() {
			field.localVariableId = localVariableId
			localVariableId++
			if field.isLong() || field.isDouble() {
				localVariableId++
			}
		}
	}
}

func calculateStaticFieldLocalVariableIds(class *Class) {
	localVariableId := uint(0)
	for _, field := range class.fields {
		if field.IsStatic() {
			field.localVariableId = localVariableId
			localVariableId++
			if field.isLong() || field.isDouble() {
				localVariableId++
			}
		}
	}
}

func allocateAndInitializeStaticLocalVariables(class *Class) {
	class.staticLocalVariableTable = newLocalVariableTable(class.staticLocalVariableTableSize)
	for _, field := range class.fields {
		if field.IsStatic() && field.IsFinal() {
			initializeStaticFinalLocalVariables(class, field)
		}
	}
}

func initializeStaticFinalLocalVariables(class *Class, field *Field) {
	staticLocalVariableTable := class.staticLocalVariableTable
	constantPool := class.constantPool
	constantValueIndex := field.constantValueIndex
	localVariableId := field.localVariableId
	if constantValueIndex > 0 {
		switch field.descriptor {
		case "I", "B", "C", "S", "Z":
			value := constantPool.GetConstant(constantValueIndex).(int32)
			staticLocalVariableTable.SetInt(localVariableId, value)
		case "D":
			value := constantPool.GetConstant(constantValueIndex).(int64)
			staticLocalVariableTable.SetLong(localVariableId, value)
		case "F":
			value := constantPool.GetConstant(constantValueIndex).(float32)
			staticLocalVariableTable.SetFloat(localVariableId, value)
		case "J":
			value := constantPool.GetConstant(constantValueIndex).(float64)
			staticLocalVariableTable.SetDouble(localVariableId, value)
		case "Ljava/lang/String":
			// TODO
		}
	}
}
