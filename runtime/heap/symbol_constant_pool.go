package heap

type SymbolConstantPool struct {
	constantPool *ConstantPool
	className    string
	class        *Class
}

func (symbolConstantPool *SymbolConstantPool) Class() *Class {
	if symbolConstantPool.class == nil {
		symbolConstantPool.resolveClass()
	}
	return symbolConstantPool.class
}

func (symbolConstantPool *SymbolConstantPool) resolveClass() {
	classSymbol := symbolConstantPool.constantPool.class
	class := classSymbol.classLoader.LoadClass(symbolConstantPool.className)
	if !class.isAccessibleTo(classSymbol) {
		panic("java.lang.IllegalAccessError")
	}
	symbolConstantPool.class = class
}
