package heap

import "bitbucket.org/nvxarm/jvm/file"

type ClassMember struct {
	AccessFlags
	name       string
	descriptor string
	class      *Class
}

func (classMember *ClassMember) copyMemberInfo(memberInfo *file.MemberInfo) {
	classMember.accessFlags = memberInfo.AccessFlags()
	classMember.name = memberInfo.Name()
	classMember.descriptor = memberInfo.Descriptor()
}

func (classMember *ClassMember) isAccessibleTo(target *Class) bool {
	if classMember.IsPublic() {
		return true
	}
	class := classMember.class
	if classMember.IsProtected() {
		return class == target || target.isSubClassOf(class) || class.getPackageName() == target.getPackageName()
	}
	if !classMember.IsPrivate() {
		return class.getPackageName() == target.getPackageName()
	}
	return class == target
}

func (classMember *ClassMember) Class() *Class {
	return classMember.class
}
