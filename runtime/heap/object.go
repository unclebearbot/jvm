package heap

type Object struct {
	class  *Class
	fields LocalVariableTable
}

func newObject(class *Class) *Object {
	return &Object{
		class:  class,
		fields: newLocalVariableTable(class.localVariableTableSize),
	}
}
