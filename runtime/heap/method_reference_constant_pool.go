package heap

import "bitbucket.org/nvxarm/jvm/file"

type MethodReferenceConstantPool struct {
	MemberReferenceConstantPool
	method *Method
}

func newMethodReferenceConstantPool(constantPool *ConstantPool, methodConstantInfo *file.MethodConstantInfo) *MethodReferenceConstantPool {
	methodReferenceConstantPool := &MethodReferenceConstantPool{}
	methodReferenceConstantPool.constantPool = constantPool
	methodReferenceConstantPool.copyMemberConstantInfo(&methodConstantInfo.MemberConstantInfo)
	return methodReferenceConstantPool
}
