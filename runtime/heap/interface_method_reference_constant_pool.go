package heap

import "bitbucket.org/nvxarm/jvm/file"

type InterfaceMethodReferenceConstantPool struct {
	MemberReferenceConstantPool
	method *Method
}

func newInterfaceMethodReferenceConstantPool(constantPool *ConstantPool, methodConstantInfo *file.MethodConstantInfo) *InterfaceMethodReferenceConstantPool {
	interfaceMethodReferenceConstantPool := &InterfaceMethodReferenceConstantPool{}
	interfaceMethodReferenceConstantPool.constantPool = constantPool
	interfaceMethodReferenceConstantPool.copyMemberConstantInfo(&methodConstantInfo.MemberConstantInfo)
	return interfaceMethodReferenceConstantPool
}
