package heap

import "bitbucket.org/nvxarm/jvm/file"

type Method struct {
	ClassMember
	threadStackCapacity    uint
	localVariableTableSize uint
	code                   []byte
}

func newMethods(class *Class, classFileMethods []*file.MemberInfo) []*Method {
	methods := make([]*Method, len(classFileMethods))
	for i, method := range classFileMethods {
		methods[i] = &Method{}
		methods[i].class = class
		methods[i].copyMemberInfo(method)
		methods[i].copyCodeAttributeInfo(method)
	}
	return methods
}

func (method *Method) copyCodeAttributeInfo(classFileMethod *file.MemberInfo) {
	if codeAttributeInfo := classFileMethod.CodeAttributeInfo(); codeAttributeInfo != nil {
		method.threadStackCapacity = codeAttributeInfo.ThreadStackCapacity()
		method.localVariableTableSize = codeAttributeInfo.LocalVariableTableSize()
		method.code = codeAttributeInfo.Code()
	}
}
