package runtime

type Stack struct {
	capacity uint
	size     uint
	topFrame *Frame
}

func newStack(capacity uint) *Stack {
	return &Stack{capacity: capacity}
}

func (stack *Stack) push(frame *Frame) {
	if stack.size >= stack.capacity {
		panic("java.lang.StackOverflowError")
	}
	if stack.topFrame != nil {
		frame.lowerFrame = stack.topFrame
	}
	stack.topFrame = frame
	stack.size++
}

func (stack *Stack) pop() *Frame {
	if stack.topFrame == nil {
		panic("popping from empty stack")
	}
	topFrame := stack.topFrame
	stack.topFrame = topFrame.lowerFrame
	topFrame.lowerFrame = nil
	stack.size--
	return topFrame
}

func (stack *Stack) top() *Frame {
	if stack.topFrame == nil {
		panic("getting top from empty stack")
	}
	return stack.topFrame
}
