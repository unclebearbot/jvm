package runtime

import (
	"math"

	"bitbucket.org/nvxarm/jvm/runtime/heap"
)

type LocalVariable struct {
	number    int32
	reference *heap.Object
}

type LocalVariableTable []LocalVariable

func newLocalVariableTable(size uint) LocalVariableTable {
	if size <= 0 {
		panic("local variable table size must be greater than 0")
	}
	return make([]LocalVariable, size)
}

func (localVariableTable LocalVariableTable) SetInt(index uint, number int32) {
	localVariableTable[index].number = number
}

func (localVariableTable LocalVariableTable) GetInt(index uint) int32 {
	return localVariableTable[index].number
}

func (localVariableTable LocalVariableTable) SetFloat(index uint, number float32) {
	bits := math.Float32bits(number)
	localVariableTable[index].number = int32(bits)
}

func (localVariableTable LocalVariableTable) GetFloat(index uint) float32 {
	bits := uint32(localVariableTable[index].number)
	return math.Float32frombits(bits)
}

func (localVariableTable LocalVariableTable) SetLong(index uint, number int64) {
	localVariableTable[index].number = int32(number)
	localVariableTable[index+1].number = int32(number >> 32)
}

func (localVariableTable LocalVariableTable) GetLong(index uint) int64 {
	low := uint32(localVariableTable[index].number)
	high := uint32(localVariableTable[index+1].number)
	return int64(high)<<32 | int64(low)
}

func (localVariableTable LocalVariableTable) SetDouble(index uint, number float64) {
	bits := math.Float64bits(number)
	localVariableTable.SetLong(index, int64(bits))
}

func (localVariableTable LocalVariableTable) GetDouble(index uint) float64 {
	bits := uint64(localVariableTable.GetLong(index))
	return math.Float64frombits(bits)
}

func (localVariableTable LocalVariableTable) SetReference(index uint, reference *heap.Object) {
	localVariableTable[index].reference = reference
}

func (localVariableTable LocalVariableTable) GetReference(index uint) *heap.Object {
	return localVariableTable[index].reference
}
