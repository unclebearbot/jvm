package runtime

import (
	"math"

	"bitbucket.org/nvxarm/jvm/runtime/heap"
)

type OperandStack struct {
	size               uint
	localVariableTable LocalVariableTable
}

func newOperandStack(size uint) *OperandStack {
	if size <= 0 {
		panic("operand stack size must be greater than 0")
	}
	return &OperandStack{localVariableTable: newLocalVariableTable(size)}
}

func (operandStack *OperandStack) PushInt(number int32) {
	operandStack.localVariableTable[operandStack.size].number = number
	operandStack.size++
}

func (operandStack *OperandStack) PopInt() int32 {
	operandStack.size--
	return operandStack.localVariableTable[operandStack.size].number
}

func (operandStack *OperandStack) PushFloat(number float32) {
	bits := math.Float32bits(number)
	operandStack.localVariableTable[operandStack.size].number = int32(bits)
	operandStack.size++
}

func (operandStack *OperandStack) PopFloat() float32 {
	operandStack.size--
	bits := uint32(operandStack.localVariableTable[operandStack.size].number)
	return math.Float32frombits(bits)
}

func (operandStack *OperandStack) PushLong(number int64) {
	operandStack.localVariableTable[operandStack.size].number = int32(number)
	operandStack.localVariableTable[operandStack.size+1].number = int32(number >> 32)
	operandStack.size += 2
}

func (operandStack *OperandStack) PopLong() int64 {
	operandStack.size -= 2
	low := uint32(operandStack.localVariableTable[operandStack.size].number)
	high := uint32(operandStack.localVariableTable[operandStack.size+1].number)
	return int64(high)<<32 | int64(low)
}

func (operandStack *OperandStack) PushDouble(number float64) {
	bits := math.Float64bits(number)
	operandStack.PushLong(int64(bits))
}

func (operandStack *OperandStack) PopDouble() float64 {
	bits := uint64(operandStack.PopLong())
	return math.Float64frombits(bits)
}

func (operandStack *OperandStack) PushReference(reference *heap.Object) {
	operandStack.localVariableTable[operandStack.size].reference = reference
	operandStack.size++
}

func (operandStack *OperandStack) PopReference() *heap.Object {
	operandStack.size--
	reference := operandStack.localVariableTable[operandStack.size].reference
	operandStack.localVariableTable[operandStack.size].reference = nil
	return reference
}

func (operandStack *OperandStack) PushLocalVariable(localVariable LocalVariable) {
	operandStack.localVariableTable[operandStack.size] = localVariable
	operandStack.size++
}

func (operandStack *OperandStack) PopLocalVariable() LocalVariable {
	operandStack.size--
	return operandStack.localVariableTable[operandStack.size]
}
