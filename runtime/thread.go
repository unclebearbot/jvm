package runtime

type Thread struct {
	programCounter int
	stack          *Stack
}

func NewThread(stackCapacity uint) *Thread {
	return &Thread{stack: newStack(stackCapacity)}
}

func (thread *Thread) NewFrame(localVariableTableSize uint, threadStackCapacity uint) *Frame {
	return newFrame(thread, localVariableTableSize, threadStackCapacity)
}

func (thread *Thread) PushFrame(frame *Frame) {
	thread.stack.push(frame)
}

func (thread *Thread) PopFrame() *Frame {
	return thread.stack.pop()
}

func (thread *Thread) CurrentFrame() *Frame {
	return thread.stack.top()
}

func (thread *Thread) ProgramCounter() int {
	return thread.programCounter
}

func (thread *Thread) SetProgramCounter(programCounter int) {
	thread.programCounter = programCounter
}
