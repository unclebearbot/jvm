package runtime

import "bitbucket.org/nvxarm/jvm/runtime/heap"

type Frame struct {
	lowerFrame         *Frame
	thread             *Thread
	method             *heap.Method
	localVariableTable LocalVariableTable
	operandStack       *OperandStack
	nextProgramCounter int
}

func newFrame(thread *Thread, localVariableTableSize uint, threadStackCapacity uint) *Frame {
	return &Frame{
		thread:             thread,
		localVariableTable: newLocalVariableTable(localVariableTableSize),
		operandStack:       newOperandStack(threadStackCapacity),
	}
}

func (frame *Frame) Thread() *Thread {
	return frame.thread
}

func (frame *Frame) Method() *heap.Method {
	return frame.method
}

func (frame *Frame) LocalVariableTable() LocalVariableTable {
	return frame.localVariableTable
}

func (frame *Frame) OperandStack() *OperandStack {
	return frame.operandStack
}

func (frame *Frame) NextProgramCounter() int {
	return frame.nextProgramCounter
}

func (frame *Frame) SetNextProgramCounter(nextProgramCounter int) {
	frame.nextProgramCounter = nextProgramCounter
}
