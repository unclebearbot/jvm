package interpreter

import (
	"bitbucket.org/nvxarm/jvm/file"
	"bitbucket.org/nvxarm/jvm/instruction"
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

func Interpret(memberInfo *file.MemberInfo) {
	codeAttributeInfo := memberInfo.CodeAttributeInfo()
	localVariableTableSize := codeAttributeInfo.LocalVariableTableSize()
	threadStackCapacity := codeAttributeInfo.ThreadStackCapacity()
	code := codeAttributeInfo.Code()
	thread := runtime.NewThread(16)
	frame := thread.NewFrame(localVariableTableSize, threadStackCapacity)
	thread.PushFrame(frame)
	defer catch(frame)
	loop(thread, code)
}

func catch(frame *runtime.Frame) {
	if r := recover(); r != nil {
		panic(r)
	}
}

func loop(thread *runtime.Thread, code []byte) {
	frame := thread.PopFrame()
	byteCodeReader := &common.ByteCodeReader{}
	for {
		programCounter := frame.NextProgramCounter()
		thread.SetProgramCounter(programCounter)
		byteCodeReader.Reset(code, programCounter)
		operand := byteCodeReader.ReadUint8()
		i := instruction.NewInstruction(operand)
		i.FetchOperands(byteCodeReader)
		frame.SetNextProgramCounter(byteCodeReader.ProgramCounter())
		i.Execute(frame)
	}
}
