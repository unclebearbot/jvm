package conversion

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type F2D struct {
	common.NoOperandsInstruction
}

func (f2D *F2D) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopFloat()
	result := float64(value)
	stack.PushDouble(result)
}

type F2I struct {
	common.NoOperandsInstruction
}

func (f2I *F2I) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopFloat()
	result := int32(value)
	stack.PushInt(result)
}

type F2L struct {
	common.NoOperandsInstruction
}

func (f2L *F2L) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopFloat()
	result := int64(value)
	stack.PushLong(result)
}
