package conversion

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type L2D struct {
	common.NoOperandsInstruction
}

func (l2D *L2D) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopLong()
	result := float64(value)
	stack.PushDouble(result)
}

type L2F struct {
	common.NoOperandsInstruction
}

func (l2F *L2F) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopLong()
	result := float32(value)
	stack.PushFloat(result)
}

type L2I struct {
	common.NoOperandsInstruction
}

func (l2I *L2I) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopLong()
	result := int32(value)
	stack.PushInt(result)
}
