package conversion

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type D2F struct {
	common.NoOperandsInstruction
}

func (d2F *D2F) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopDouble()
	result := float32(value)
	stack.PushFloat(result)
}

type D2I struct {
	common.NoOperandsInstruction
}

func (d2I *D2I) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopDouble()
	result := int32(value)
	stack.PushInt(result)
}

type D2L struct {
	common.NoOperandsInstruction
}

func (d2L *D2L) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopDouble()
	result := int64(value)
	stack.PushLong(result)
}
