package conversion

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type I2B struct {
	common.NoOperandsInstruction
}

func (i2B *I2B) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopInt()
	result := int32(int8(value))
	stack.PushInt(result)
}

type I2C struct {
	common.NoOperandsInstruction
}

func (i2C *I2C) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopInt()
	result := int32(uint16(value))
	stack.PushInt(result)
}

type I2S struct {
	common.NoOperandsInstruction
}

func (i2S *I2S) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopInt()
	result := int32(int16(value))
	stack.PushInt(result)
}

type I2L struct {
	common.NoOperandsInstruction
}

func (i2L *I2L) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopInt()
	result := int64(value)
	stack.PushLong(result)
}

type I2F struct {
	common.NoOperandsInstruction
}

func (i2F *I2F) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopInt()
	result := float32(value)
	stack.PushFloat(result)
}

type I2D struct {
	common.NoOperandsInstruction
}

func (i2D *I2D) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopInt()
	result := float64(value)
	stack.PushDouble(result)
}
