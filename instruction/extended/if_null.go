package extended

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IfNull struct {
	common.BranchInstruction
}

func (ifNull *IfNull) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopReference() == nil {
		common.Branch(frame, ifNull.Offset)
	}
}

type IfNonNull struct {
	common.BranchInstruction
}

func (ifNonNull *IfNonNull) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopReference() != nil {
		common.Branch(frame, ifNonNull.Offset)
	}
}
