package extended

import (
	"strconv"

	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/instruction/control"
	"bitbucket.org/nvxarm/jvm/instruction/load"
	"bitbucket.org/nvxarm/jvm/instruction/math"
	"bitbucket.org/nvxarm/jvm/instruction/store"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type Wide struct {
	modifiedInstruction common.Instruction
}

func (wide *Wide) FetchOperands(byteCodeReader *common.ByteCodeReader) {
	operand := byteCodeReader.ReadUint8()
	switch operand {
	case 0x15:
		modifiedInstruction := &load.ILoad{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x16:
		modifiedInstruction := &load.LLoad{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x17:
		modifiedInstruction := &load.FLoad{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x18:
		modifiedInstruction := &load.DLoad{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x19:
		modifiedInstruction := &load.ALoad{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x36:
		modifiedInstruction := &store.IStore{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x37:
		modifiedInstruction := &store.LStore{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x38:
		modifiedInstruction := &store.FStore{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x39:
		modifiedInstruction := &store.DStore{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x3a:
		modifiedInstruction := &store.AStore{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	case 0x84:
		modifiedInstruction := &math.IInc{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		modifiedInstruction.Const = int32(byteCodeReader.ReadInt16())
		wide.modifiedInstruction = modifiedInstruction
	case 0xa9:
		modifiedInstruction := &control.Ret{}
		modifiedInstruction.Index = uint(byteCodeReader.ReadUint16())
		wide.modifiedInstruction = modifiedInstruction
	default:
		panic("Unsupported wide operand: " + strconv.FormatInt(int64(operand), 16))
	}
}

func (wide *Wide) Execute(frame *runtime.Frame) {
	wide.modifiedInstruction.Execute(frame)
}
