package stack

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type Pop struct {
	common.NoOperandsInstruction
}

func (pop *Pop) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	stack.PopLocalVariable()
}

type Pop2 struct {
	common.NoOperandsInstruction
}

func (pop2 *Pop2) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	stack.PopLocalVariable()
	stack.PopLocalVariable()
}
