package stack

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type Swap struct {
	common.NoOperandsInstruction
}

func (swap *Swap) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	localVariable1 := stack.PopLocalVariable()
	localVariable2 := stack.PopLocalVariable()
	stack.PushLocalVariable(localVariable1)
	stack.PushLocalVariable(localVariable2)
}
