package stack

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type Dup struct {
	common.NoOperandsInstruction
}

func (dup *Dup) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	localVariable := stack.PopLocalVariable()
	stack.PushLocalVariable(localVariable)
	stack.PushLocalVariable(localVariable)
}

type DupX1 struct {
	common.NoOperandsInstruction
}

func (dupX1 *DupX1) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value1 := stack.PopLocalVariable()
	value2 := stack.PopLocalVariable()
	stack.PushLocalVariable(value1)
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
}

type DupX2 struct {
	common.NoOperandsInstruction
}

func (dupX2 *DupX2) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value1 := stack.PopLocalVariable()
	value2 := stack.PopLocalVariable()
	value3 := stack.PopLocalVariable()
	stack.PushLocalVariable(value1)
	stack.PushLocalVariable(value3)
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
}

type Dup2 struct {
	common.NoOperandsInstruction
}

func (dup2 *Dup2) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value1 := stack.PopLocalVariable()
	value2 := stack.PopLocalVariable()
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
}

type Dup2X1 struct {
	common.NoOperandsInstruction
}

func (dup2X1 *Dup2X1) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value1 := stack.PopLocalVariable()
	value2 := stack.PopLocalVariable()
	value3 := stack.PopLocalVariable()
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
	stack.PushLocalVariable(value3)
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
}

type Dup2X2 struct {
	common.NoOperandsInstruction
}

func (dup2X2 *Dup2X2) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value1 := stack.PopLocalVariable()
	value2 := stack.PopLocalVariable()
	value3 := stack.PopLocalVariable()
	value4 := stack.PopLocalVariable()
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
	stack.PushLocalVariable(value4)
	stack.PushLocalVariable(value3)
	stack.PushLocalVariable(value2)
	stack.PushLocalVariable(value1)
}
