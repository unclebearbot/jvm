package load

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type ILoad struct{ common.Index8Instruction }

func (iLoad *ILoad) Execute(frame *runtime.Frame) {
	loadInt(frame, uint(iLoad.Index))
}

type ILoad0 struct {
	common.NoOperandsInstruction
}

func (iLoad0 *ILoad0) Execute(frame *runtime.Frame) {
	loadInt(frame, 0)
}

type ILoad1 struct {
	common.NoOperandsInstruction
}

func (iLoad1 *ILoad1) Execute(frame *runtime.Frame) {
	loadInt(frame, 1)
}

type ILoad2 struct {
	common.NoOperandsInstruction
}

func (iLoad2 *ILoad2) Execute(frame *runtime.Frame) {
	loadInt(frame, 2)
}

type ILoad3 struct {
	common.NoOperandsInstruction
}

func (iLoad3 *ILoad3) Execute(frame *runtime.Frame) {
	loadInt(frame, 3)
}

func loadInt(frame *runtime.Frame, index uint) {
	value := frame.LocalVariableTable().GetInt(index)
	frame.OperandStack().PushInt(value)
}
