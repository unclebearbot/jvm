package load

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type FLoad struct{ common.Index8Instruction }

func (fLoad *FLoad) Execute(frame *runtime.Frame) {
	loadFloat(frame, uint(fLoad.Index))
}

type FLoad0 struct {
	common.NoOperandsInstruction
}

func (fLoad0 *FLoad0) Execute(frame *runtime.Frame) {
	loadFloat(frame, 0)
}

type FLoad1 struct {
	common.NoOperandsInstruction
}

func (fLoad1 *FLoad1) Execute(frame *runtime.Frame) {
	loadFloat(frame, 1)
}

type FLoad2 struct {
	common.NoOperandsInstruction
}

func (fLoad2 *FLoad2) Execute(frame *runtime.Frame) {
	loadFloat(frame, 2)
}

type FLoad3 struct {
	common.NoOperandsInstruction
}

func (fLoad3 *FLoad3) Execute(frame *runtime.Frame) {
	loadFloat(frame, 3)
}

func loadFloat(frame *runtime.Frame, index uint) {
	value := frame.LocalVariableTable().GetFloat(index)
	frame.OperandStack().PushFloat(value)
}
