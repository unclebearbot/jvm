package load

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type LLoad struct{ common.Index8Instruction }

func (lLoad *LLoad) Execute(frame *runtime.Frame) {
	loadLong(frame, uint(lLoad.Index))
}

type LLoad0 struct {
	common.NoOperandsInstruction
}

func (lLoad0 *LLoad0) Execute(frame *runtime.Frame) {
	loadLong(frame, 0)
}

type LLoad1 struct {
	common.NoOperandsInstruction
}

func (lLoad1 *LLoad1) Execute(frame *runtime.Frame) {
	loadLong(frame, 1)
}

type LLoad2 struct {
	common.NoOperandsInstruction
}

func (lLoad2 *LLoad2) Execute(frame *runtime.Frame) {
	loadLong(frame, 2)
}

type LLoad3 struct {
	common.NoOperandsInstruction
}

func (lLoad3 *LLoad3) Execute(frame *runtime.Frame) {
	loadLong(frame, 3)
}

func loadLong(frame *runtime.Frame, index uint) {
	value := frame.LocalVariableTable().GetLong(index)
	frame.OperandStack().PushLong(value)
}
