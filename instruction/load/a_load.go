package load

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type ALoad struct{ common.Index8Instruction }

func (aLoad *ALoad) Execute(frame *runtime.Frame) {
	loadReference(frame, uint(aLoad.Index))
}

type ALoad0 struct {
	common.NoOperandsInstruction
}

func (aLoad0 *ALoad0) Execute(frame *runtime.Frame) {
	loadReference(frame, 0)
}

type ALoad1 struct {
	common.NoOperandsInstruction
}

func (aLoad1 *ALoad1) Execute(frame *runtime.Frame) {
	loadReference(frame, 1)
}

type ALoad2 struct {
	common.NoOperandsInstruction
}

func (aLoad2 *ALoad2) Execute(frame *runtime.Frame) {
	loadReference(frame, 2)
}

type ALoad3 struct {
	common.NoOperandsInstruction
}

func (aLoad3 *ALoad3) Execute(frame *runtime.Frame) {
	loadReference(frame, 3)
}

func loadReference(frame *runtime.Frame, index uint) {
	reference := frame.LocalVariableTable().GetReference(index)
	frame.OperandStack().PushReference(reference)
}
