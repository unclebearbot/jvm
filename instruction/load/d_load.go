package load

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DLoad struct{ common.Index8Instruction }

func (dLoad *DLoad) Execute(frame *runtime.Frame) {
	loadDouble(frame, uint(dLoad.Index))
}

type DLoad0 struct {
	common.NoOperandsInstruction
}

func (dLoad0 *DLoad0) Execute(frame *runtime.Frame) {
	loadDouble(frame, 0)
}

type DLoad1 struct {
	common.NoOperandsInstruction
}

func (dLoad1 *DLoad1) Execute(frame *runtime.Frame) {
	loadDouble(frame, 1)
}

type DLoad2 struct {
	common.NoOperandsInstruction
}

func (dLoad2 *DLoad2) Execute(frame *runtime.Frame) {
	loadDouble(frame, 2)
}

type DLoad3 struct {
	common.NoOperandsInstruction
}

func (dLoad3 *DLoad3) Execute(frame *runtime.Frame) {
	loadDouble(frame, 3)
}

func loadDouble(frame *runtime.Frame, index uint) {
	value := frame.LocalVariableTable().GetDouble(index)
	frame.OperandStack().PushDouble(value)
}
