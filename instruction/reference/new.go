package reference

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
	"bitbucket.org/nvxarm/jvm/runtime/heap"
)

type New struct {
	common.Index16Instruction
}

func (new New) Execute(frame *runtime.Frame) {
	constantPool := frame.Method().Class().ConstantPool()
	classReference := constantPool.GetConstant(new.Index).(*heap.ClassReferenceConstantPool)
	class := classReference.Class()
	if class.IsInterface() || class.IsAbstract() {
		panic("java.lang.InstantiationError")
	}
	reference := class.NewObject()
	frame.OperandStack().PushReference(reference)
}
