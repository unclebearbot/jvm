package reference

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
	"bitbucket.org/nvxarm/jvm/runtime/heap"
)

type PutStatic struct {
	common.Index16Instruction
}

func (putStatic *PutStatic) Execute(frame *runtime.Frame) {
	method := frame.Method()
	class := method.Class()
	constantPool := class.ConstantPool()
	reference := constantPool.GetConstant(putStatic.index).(*heap.FieldReferenceConstantPool)
	field := reference.Field()
	if field.IsStatic() {
		panic("java.lang.IncompatibleClassChangeError")
	}
	if field.IsFinal() {
		if class != field.Class() || method.Name() != "<clinit>" {
			panic("java.lang.IllegalAccessError")
		}
	}
	descriptor := field.Descriptor()
	localVariableId := field.LocalVariableId()
	switch descriptor[0] {
	case "I", "B", "C", "S", "Z":
		value := stack.PopInt()
		fieldReference := stack.PopReference()
		if fieldReference == nil {
			panic("java.lang.NullPointerException")
		}
		fieldReference.Fields().SetInt(localVariableId, value)
	case "D":
	case "F":
	case "J":
	case "L", "[":
	}
}
