package reference

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
	"bitbucket.org/nvxarm/jvm/runtime/heap"
)

type GetStatic struct {
	common.Index16Instruction
}

func (getStatic *GetStatic) Execute(frame *runtime.Frame) {
	constantPool := frame.Method().Class().ConstantPool()
	fieldReference := constantPool.GetConstant(getStatic.index).(*heap.FieldReferenceConstantPool)
	field := fieldReference.Field()
	class := field.Class()
	if !field.IsStatic() {
		panic("java.lang.IncompatibleClassChangeError")
	}
	descriptor := field.Descriptor()
	localVariableId := field.LocalVariableId()
	staticLocalVariableTable := class.StaticLocalVariableTable()
	stack := frame.OperandStack()
	switch descriptor[0] {
	case "I", "B", "C", "S", "Z":
		staticLocalVariableTable.SetInt(localVariableId, stack.PopInt())
	case "D":
		staticLocalVariableTable.SetDouble(localVariableId, stack.PopDouble())
	case "F":
		staticLocalVariableTable.SetFloat(localVariableId, stack.PopFloat())
	case "J":
		staticLocalVariableTable.SetLong(localVariableId, stack.PopLong())
	case "L", "[":
		staticLocalVariableTable.SetReference(localVariableId, stack.PopReference())
	}
}
