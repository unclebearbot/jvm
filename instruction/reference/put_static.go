package reference

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
	"bitbucket.org/nvxarm/jvm/runtime/heap"
)

type PutStatic struct {
	common.Index16Instruction
}

func (putStatic *PutStatic) Execute(frame *runtime.Frame) {
	method := frame.Method()
	currentClass = method.Class()
	constantPool := currentClass.ConstantPool()
	reference := constantPool.GetConstant(putStatic.Index).(*heap.FieldReferenceConstantPool)
	field := reference.Field()
	class := field.Class()
	if !field.IsStatic() {
		panic("java.lang.IncompatibleClassChangeError")
	}
	if field.IsFinal() {
		if class != currentClass || method.Name() != "<clinit>" {
			panic("java.lang.IllegalAccessError")
		}
	}
	descriptor := field.Descriptor()
	localVariableId := field.LocalVariableId()
	staticLocalVariableTable := class.StaticLocalVariableTable()
	stack := frame.OperandStack()
	switch descriptor[0] {
	case "I", "B", "C", "S", "Z":
		stack.PushInt(staticLocalVariableTable.GetInt(localVariableId))
	case "D":
		stack.PushDouble(staticLocalVariableTable.GetDouble(localVariableId))
	case "F":
		stack.PushFloat(staticLocalVariableTable.GetFloat(localVariableId))
	case "J":
		stack.PushLong(staticLocalVariableTable.GetLong(localVariableId))
	case "L", "[":
		stack.PushReference(staticLocalVariableTable.GetReference(localVariableId))
	}
}
