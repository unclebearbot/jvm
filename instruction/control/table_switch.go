package control

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type TableSwitch struct {
	defaultOffset int32
	low           int32
	high          int32
	jumpOffsets   []int32
}

func (tableSwitch *TableSwitch) FetchOperands(byteCodeReader *common.ByteCodeReader) {
	byteCodeReader.SkipPadding()
	tableSwitch.defaultOffset = byteCodeReader.ReadInt32()
	tableSwitch.low = byteCodeReader.ReadInt32()
	tableSwitch.high = byteCodeReader.ReadInt32()
	jumpOffsetCount := tableSwitch.high - tableSwitch.low + 1
	tableSwitch.jumpOffsets = byteCodeReader.ReadInt32s(jumpOffsetCount)
}

func (tableSwitch *TableSwitch) Execute(frame *runtime.Frame) {
	index := frame.OperandStack().PopInt()
	if tableSwitch.low <= index && index <= tableSwitch.high {
		common.Branch(frame, int(tableSwitch.jumpOffsets[index-tableSwitch.low]))
	} else {
		common.Branch(frame, int(tableSwitch.defaultOffset))
	}
}
