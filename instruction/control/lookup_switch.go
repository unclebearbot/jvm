package control

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type LookupSwitch struct {
	defaultOffset int32
	pairCount     int32
	matchOffsets  []int32
}

func (lookupSwitch *LookupSwitch) FetchOperands(byteCodeReader *common.ByteCodeReader) {
	byteCodeReader.SkipPadding()
	lookupSwitch.defaultOffset = byteCodeReader.ReadInt32()
	lookupSwitch.pairCount = byteCodeReader.ReadInt32()
	lookupSwitch.matchOffsets = byteCodeReader.ReadInt32s(lookupSwitch.pairCount * 2)
}

func (lookupSwitch *LookupSwitch) Execute(frame *runtime.Frame) {
	key := frame.OperandStack().PopInt()
	for i := int32(0); i < lookupSwitch.pairCount*2; i += 2 {
		if lookupSwitch.matchOffsets[i] == key {
			offset := lookupSwitch.matchOffsets[i+1]
			common.Branch(frame, int(offset))
			return
		}
	}
	common.Branch(frame, int(lookupSwitch.defaultOffset))
}
