package control

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type GoTo struct {
	common.BranchInstruction
}

func (goTo *GoTo) Execute(frame *runtime.Frame) {
	common.Branch(frame, goTo.Offset)
}

type GoToW struct {
	offset int
}

func (goToW *GoToW) FetchOperands(byteCodeReader *common.ByteCodeReader) {
	goToW.offset = int(byteCodeReader.ReadInt32())
}

func (goToW *GoToW) Execute(frame *runtime.Frame) {
	common.Branch(frame, goToW.offset)
}
