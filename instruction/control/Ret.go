package control

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type Ret struct {
	common.Index8Instruction
}

func (ret *Ret) Execute(frame *runtime.Frame) {
	panic("Unsupported instruction")
}
