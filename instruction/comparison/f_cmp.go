package comparison

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type FCmpG struct {
	common.NoOperandsInstruction
}

func (fCmpG *FCmpG) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	float1 := stack.PopFloat()
	float2 := stack.PopFloat()
	if float1 < float2 {
		stack.PushInt(1)
		return
	}
	if float1 > float2 {
		stack.PushInt(-1)
		return
	}
	if float1 == float2 {
		stack.PushInt(0)
		return
	}
	stack.PushInt(1)
}

type FCmpL struct {
	common.NoOperandsInstruction
}

func (fCmpL *FCmpL) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	float1 := stack.PopFloat()
	float2 := stack.PopFloat()
	if float1 < float2 {
		stack.PushInt(1)
		return
	}
	if float1 > float2 {
		stack.PushInt(-1)
		return
	}
	if float1 == float2 {
		stack.PushInt(0)
		return
	}
	stack.PushInt(-1)
}
