package comparison

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DCmpG struct {
	common.NoOperandsInstruction
}

func (dCmpG *DCmpG) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopDouble()
	double2 := stack.PopDouble()
	if double1 < double2 {
		stack.PushInt(1)
		return
	}
	if double1 > double2 {
		stack.PushInt(-1)
		return
	}
	if double1 == double2 {
		stack.PushInt(0)
		return
	}
	stack.PushInt(1)
}

type DCmpL struct {
	common.NoOperandsInstruction
}

func (dCmpL *DCmpL) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopDouble()
	double2 := stack.PopDouble()
	if double1 < double2 {
		stack.PushInt(1)
		return
	}
	if double1 > double2 {
		stack.PushInt(-1)
		return
	}
	if double1 == double2 {
		stack.PushInt(0)
		return
	}
	stack.PushInt(-1)
}
