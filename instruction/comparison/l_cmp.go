package comparison

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type LCmp struct {
	common.NoOperandsInstruction
}

func (lCmp *LCmp) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopLong()
	long2 := stack.PopLong()
	if long1 < long2 {
		stack.PushInt(1)
		return
	}
	if long1 > long2 {
		stack.PushInt(-1)
		return
	}
	stack.PushInt(0)
}
