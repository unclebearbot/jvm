package comparison

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IfICmpEq struct {
	common.BranchInstruction
}

func (ifICmpEq *IfICmpEq) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int2 == int1 {
		common.Branch(frame, ifICmpEq.Offset)
	}
}

type IfICmpNE struct {
	common.BranchInstruction
}

func (ifICmpNE *IfICmpNE) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int2 != int1 {
		common.Branch(frame, ifICmpNE.Offset)
	}
}

type IfICmpLT struct {
	common.BranchInstruction
}

func (ifICmpLT *IfICmpLT) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int2 < int1 {
		common.Branch(frame, ifICmpLT.Offset)
	}
}

type IfICmpLE struct {
	common.BranchInstruction
}

func (ifICmpLE *IfICmpLE) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int2 <= int1 {
		common.Branch(frame, ifICmpLE.Offset)
	}
}

type IfICmpGT struct {
	common.BranchInstruction
}

func (ifICmpGT *IfICmpGT) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int2 > int1 {
		common.Branch(frame, ifICmpGT.Offset)
	}
}

type IfICmpGE struct {
	common.BranchInstruction
}

func (ifICmpGE *IfICmpGE) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int2 >= int1 {
		common.Branch(frame, ifICmpGE.Offset)
	}
}
