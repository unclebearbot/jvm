package comparison

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IfACmpEq struct {
	common.BranchInstruction
}

func (ifACmpEq *IfACmpEq) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	if stack.PopReference() == stack.PopReference() {
		common.Branch(frame, ifACmpEq.Offset)
	}
}

type IfACmpNE struct {
	common.BranchInstruction
}

func (ifACmpNE *IfACmpNE) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	if stack.PopReference() != stack.PopReference() {
		common.Branch(frame, ifACmpNE.Offset)
	}
}
