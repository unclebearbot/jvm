package comparison

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IfEq struct {
	common.BranchInstruction
}

func (ifEq *IfEq) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopInt() == 0 {
		common.Branch(frame, ifEq.Offset)
	}
}

type IfNE struct {
	common.BranchInstruction
}

func (ifNE *IfNE) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopInt() != 0 {
		common.Branch(frame, ifNE.Offset)
	}
}

type IfLT struct {
	common.BranchInstruction
}

func (ifLT *IfLT) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopInt() < 0 {
		common.Branch(frame, ifLT.Offset)
	}
}

type IfLE struct {
	common.BranchInstruction
}

func (ifLE *IfLE) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopInt() <= 0 {
		common.Branch(frame, ifLE.Offset)
	}
}

type IfGT struct {
	common.BranchInstruction
}

func (ifGT *IfGT) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopInt() > 0 {
		common.Branch(frame, ifGT.Offset)
	}
}

type IfGE struct {
	common.BranchInstruction
}

func (ifGE *IfGE) Execute(frame *runtime.Frame) {
	if frame.OperandStack().PopInt() >= 0 {
		common.Branch(frame, ifGE.Offset)
	}
}
