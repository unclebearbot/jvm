package common

type ByteCodeReader struct {
	code           []byte
	programCounter int
}

func (byteCodeReader *ByteCodeReader) Reset(code []byte, programCounter int) {
	byteCodeReader.code = code
	byteCodeReader.programCounter = programCounter
}

func (byteCodeReader *ByteCodeReader) ReadInt8() int8 {
	return int8(byteCodeReader.ReadUint8())
}

func (byteCodeReader *ByteCodeReader) ReadUint8() uint8 {
	i := byteCodeReader.code[byteCodeReader.programCounter]
	byteCodeReader.programCounter++
	return i
}

func (byteCodeReader *ByteCodeReader) ReadInt16() int16 {
	return int16(byteCodeReader.ReadUint16())
}

func (byteCodeReader *ByteCodeReader) ReadUint16() uint16 {
	high := uint16(byteCodeReader.ReadUint8())
	low := uint16(byteCodeReader.ReadUint8())
	return (high << 8) | low
}

func (byteCodeReader *ByteCodeReader) ReadInt32() int32 {
	byte1 := int32(byteCodeReader.ReadUint8())
	byte2 := int32(byteCodeReader.ReadUint8())
	byte3 := int32(byteCodeReader.ReadUint8())
	byte4 := int32(byteCodeReader.ReadUint8())
	return (byte1 << 24) | (byte2 << 16) | (byte3 << 8) | byte4
}

func (byteCodeReader *ByteCodeReader) ReadInt32s(count int32) []int32 {
	int32s := make([]int32, count)
	for i := range int32s {
		int32s[i] = byteCodeReader.ReadInt32()
	}
	return int32s
}

func (byteCodeReader *ByteCodeReader) SkipPadding() {
	for byteCodeReader.programCounter%4 != 0 {
		byteCodeReader.ReadUint8()
	}
}

func (byteCodeReader *ByteCodeReader) ProgramCounter() int {
	return byteCodeReader.programCounter
}
