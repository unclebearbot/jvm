package common

import "bitbucket.org/nvxarm/jvm/runtime"

type Instruction interface {
	FetchOperands(byteCodeReader *ByteCodeReader)
	Execute(frame *runtime.Frame)
}

type NoOperandsInstruction struct{}

func (noOperandsInstruction *NoOperandsInstruction) FetchOperands(byteCodeReader *ByteCodeReader) {}

type BranchInstruction struct {
	Offset int
}

func (branchInstruction *BranchInstruction) FetchOperands(byteCodeReader *ByteCodeReader) {
	branchInstruction.Offset = int(byteCodeReader.ReadInt16())
}

type Index8Instruction struct {
	Index uint
}

func (index8Instruction *Index8Instruction) FetchOperands(byteCodeReader *ByteCodeReader) {
	index8Instruction.Index = uint(byteCodeReader.ReadUint8())
}

type Index16Instruction struct {
	Index uint
}

func (index16Instruction *Index16Instruction) FetchOperands(byteCodeReader *ByteCodeReader) {
	index16Instruction.Index = uint(byteCodeReader.ReadUint16())
}
