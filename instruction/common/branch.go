package common

import (
	"bitbucket.org/nvxarm/jvm/runtime"
)

func Branch(frame *runtime.Frame, offset int) {
	programCounter := frame.Thread().ProgramCounter()
	nextProgramCounter := programCounter + offset
	frame.SetNextProgramCounter(nextProgramCounter)
}
