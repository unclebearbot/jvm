package constant

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type NOp struct {
	common.NoOperandsInstruction
}

func (nOp *NOp) Execute(frame *runtime.Frame) {}
