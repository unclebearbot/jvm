package store

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type LStore struct{ common.Index8Instruction }

func (lStore *LStore) Execute(frame *runtime.Frame) {
	storeLong(frame, uint(lStore.Index))
}

type LStore0 struct {
	common.NoOperandsInstruction
}

func (lStore0 *LStore0) Execute(frame *runtime.Frame) {
	storeLong(frame, 0)
}

type LStore1 struct {
	common.NoOperandsInstruction
}

func (lStore1 *LStore1) Execute(frame *runtime.Frame) {
	storeLong(frame, 1)
}

type LStore2 struct {
	common.NoOperandsInstruction
}

func (lStore2 *LStore2) Execute(frame *runtime.Frame) {
	storeLong(frame, 2)
}

type LStore3 struct {
	common.NoOperandsInstruction
}

func (lStore3 *LStore3) Execute(frame *runtime.Frame) {
	storeLong(frame, 3)
}

func storeLong(frame *runtime.Frame, index uint) {
	value := frame.OperandStack().PopLong()
	frame.LocalVariableTable().SetLong(index, value)
}
