package store

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DStore struct{ common.Index8Instruction }

func (dStore *DStore) Execute(frame *runtime.Frame) {
	storeDouble(frame, uint(dStore.Index))
}

type DStore0 struct {
	common.NoOperandsInstruction
}

func (dStore0 *DStore0) Execute(frame *runtime.Frame) {
	storeDouble(frame, 0)
}

type DStore1 struct {
	common.NoOperandsInstruction
}

func (dStore1 *DStore1) Execute(frame *runtime.Frame) {
	storeDouble(frame, 1)
}

type DStore2 struct {
	common.NoOperandsInstruction
}

func (dStore2 *DStore2) Execute(frame *runtime.Frame) {
	storeDouble(frame, 2)
}

type DStore3 struct {
	common.NoOperandsInstruction
}

func (dStore3 *DStore3) Execute(frame *runtime.Frame) {
	storeDouble(frame, 3)
}

func storeDouble(frame *runtime.Frame, index uint) {
	value := frame.OperandStack().PopDouble()
	frame.LocalVariableTable().SetDouble(index, value)
}
