package store

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type FStore struct{ common.Index8Instruction }

func (fStore *FStore) Execute(frame *runtime.Frame) {
	storeFloat(frame, uint(fStore.Index))
}

type FStore0 struct {
	common.NoOperandsInstruction
}

func (fStore0 *FStore0) Execute(frame *runtime.Frame) {
	storeFloat(frame, 0)
}

type FStore1 struct {
	common.NoOperandsInstruction
}

func (fStore1 *FStore1) Execute(frame *runtime.Frame) {
	storeFloat(frame, 1)
}

type FStore2 struct {
	common.NoOperandsInstruction
}

func (fStore2 *FStore2) Execute(frame *runtime.Frame) {
	storeFloat(frame, 2)
}

type FStore3 struct {
	common.NoOperandsInstruction
}

func (fStore3 *FStore3) Execute(frame *runtime.Frame) {
	storeFloat(frame, 3)
}

func storeFloat(frame *runtime.Frame, index uint) {
	value := frame.OperandStack().PopFloat()
	frame.LocalVariableTable().SetFloat(index, value)
}
