package store

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type AStore struct{ common.Index8Instruction }

func (aStore *AStore) Execute(frame *runtime.Frame) {
	storeReference(frame, uint(aStore.Index))
}

type AStore0 struct {
	common.NoOperandsInstruction
}

func (aStore0 *AStore0) Execute(frame *runtime.Frame) {
	storeReference(frame, 0)
}

type AStore1 struct {
	common.NoOperandsInstruction
}

func (aStore1 *AStore1) Execute(frame *runtime.Frame) {
	storeReference(frame, 1)
}

type AStore2 struct {
	common.NoOperandsInstruction
}

func (aStore2 *AStore2) Execute(frame *runtime.Frame) {
	storeReference(frame, 2)
}

type AStore3 struct {
	common.NoOperandsInstruction
}

func (aStore3 *AStore3) Execute(frame *runtime.Frame) {
	storeReference(frame, 3)
}

func storeReference(frame *runtime.Frame, index uint) {
	reference := frame.OperandStack().PopReference()
	frame.LocalVariableTable().SetReference(index, reference)
}
