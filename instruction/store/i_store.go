package store

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IStore struct{ common.Index8Instruction }

func (iStore *IStore) Execute(frame *runtime.Frame) {
	storeInt(frame, uint(iStore.Index))
}

type IStore0 struct {
	common.NoOperandsInstruction
}

func (iStore0 *IStore0) Execute(frame *runtime.Frame) {
	storeInt(frame, 0)
}

type IStore1 struct {
	common.NoOperandsInstruction
}

func (iStore1 *IStore1) Execute(frame *runtime.Frame) {
	storeInt(frame, 1)
}

type IStore2 struct {
	common.NoOperandsInstruction
}

func (iStore2 *IStore2) Execute(frame *runtime.Frame) {
	storeInt(frame, 2)
}

type IStore3 struct {
	common.NoOperandsInstruction
}

func (iStore3 *IStore3) Execute(frame *runtime.Frame) {
	storeInt(frame, 3)
}

func storeInt(frame *runtime.Frame, index uint) {
	value := frame.OperandStack().PopInt()
	frame.LocalVariableTable().SetInt(index, value)
}
