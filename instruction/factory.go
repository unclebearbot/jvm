package instruction

import (
	"strconv"

	. "bitbucket.org/nvxarm/jvm/instruction/common"
	. "bitbucket.org/nvxarm/jvm/instruction/comparison"
	. "bitbucket.org/nvxarm/jvm/instruction/constant"
	. "bitbucket.org/nvxarm/jvm/instruction/control"
	. "bitbucket.org/nvxarm/jvm/instruction/conversion"
	. "bitbucket.org/nvxarm/jvm/instruction/extended"
	. "bitbucket.org/nvxarm/jvm/instruction/load"
	. "bitbucket.org/nvxarm/jvm/instruction/math"
	. "bitbucket.org/nvxarm/jvm/instruction/push"
	. "bitbucket.org/nvxarm/jvm/instruction/stack"
	. "bitbucket.org/nvxarm/jvm/instruction/store"
)

var (
	nOp        = &NOp{}
	aConstNull = &AConstNull{}
	iConstM1   = &IConstM1{}
	iConst0    = &IConst0{}
	iConst1    = &IConst1{}
	iConst2    = &IConst2{}
	iConst3    = &IConst3{}
	iConst4    = &IConst4{}
	iConst5    = &IConst5{}
	lConst0    = &LConst0{}
	lConst1    = &LConst1{}
	fConst0    = &FConst0{}
	fConst1    = &FConst1{}
	fConst2    = &FConst2{}
	dConst0    = &DConst0{}
	dConst1    = &DConst1{}
	iLoad0     = &ILoad0{}
	iLoad1     = &ILoad1{}
	iLoad2     = &ILoad2{}
	iLoad3     = &ILoad3{}
	lLoad0     = &LLoad0{}
	lLoad1     = &LLoad1{}
	lLoad2     = &LLoad2{}
	lLoad3     = &LLoad3{}
	fLoad0     = &FLoad0{}
	fLoad1     = &FLoad1{}
	fLoad2     = &FLoad2{}
	fLoad3     = &FLoad3{}
	dLoad0     = &DLoad0{}
	dLoad1     = &DLoad1{}
	dLoad2     = &DLoad2{}
	dLoad3     = &DLoad3{}
	aLoad0     = &ALoad0{}
	aLoad1     = &ALoad1{}
	aLoad2     = &ALoad2{}
	aLoad3     = &ALoad3{}
	// iALoad = &IALoad{}
	// lALoad = &LALoad{}
	// fALoad = &FALoad{}
	// dALoad = &DALoad{}
	// aALoad = &AALoad{}
	// bALoad = &BALoad{}
	// cALoad = &CALoad{}
	// sALoad = &SALoad{}
	iStore0 = &IStore0{}
	iStore1 = &IStore1{}
	iStore2 = &IStore2{}
	iStore3 = &IStore3{}
	lStore0 = &LStore0{}
	lStore1 = &LStore1{}
	lStore2 = &LStore2{}
	lStore3 = &LStore3{}
	fStore0 = &FStore0{}
	fStore1 = &FStore1{}
	fStore2 = &FStore2{}
	fStore3 = &FStore3{}
	dStore0 = &DStore0{}
	dStore1 = &DStore1{}
	dStore2 = &DStore2{}
	dStore3 = &DStore3{}
	aStore0 = &AStore0{}
	aStore1 = &AStore1{}
	aStore2 = &AStore2{}
	aStore3 = &AStore3{}
	// iAStore = &IAStore{}
	// lAStore = &LAStore{}
	// fAStore = &FAStore{}
	// dAStore = &DAStore{}
	// aAStore = &AAStore{}
	// bAStore = &BAStore{}
	// cAStore = &CAStore{}
	// sAStore = &SAStore{}
	pop    = &Pop{}
	pop2   = &Pop2{}
	dup    = &Dup{}
	dupX1  = &DupX1{}
	dupX2  = &DupX2{}
	dup2   = &Dup2{}
	dup2X1 = &Dup2X1{}
	dup2X2 = &Dup2X2{}
	swap   = &Swap{}
	iAdd   = &IAdd{}
	lAdd   = &LAdd{}
	fAdd   = &FAdd{}
	dAdd   = &DAdd{}
	iSub   = &ISub{}
	lSub   = &LSub{}
	fSub   = &FSub{}
	dSub   = &DSub{}
	iMul   = &IMul{}
	lMul   = &LMul{}
	fMul   = &FMul{}
	dMul   = &DMul{}
	iDiv   = &IDiv{}
	lDiv   = &LDiv{}
	fDiv   = &FDiv{}
	dDiv   = &DDiv{}
	iRem   = &IRem{}
	lRem   = &LRem{}
	fRem   = &FRem{}
	dRem   = &DRem{}
	iNeg   = &INeg{}
	lNeg   = &LNeg{}
	fNeg   = &FNeg{}
	dNeg   = &DNeg{}
	iShL   = &IShL{}
	lShL   = &LShL{}
	iShR   = &IShR{}
	lShR   = &LShR{}
	iuShR  = &IUShR{}
	luShR  = &LUShR{}
	iAnd   = &IAnd{}
	lAnd   = &LAnd{}
	iOr    = &IOr{}
	lOr    = &LOr{}
	iXOr   = &IXOr{}
	lXOr   = &LXOr{}
	i2L    = &I2L{}
	i2F    = &I2F{}
	i2D    = &I2D{}
	l2I    = &L2I{}
	l2F    = &L2F{}
	l2D    = &L2D{}
	f2I    = &F2I{}
	f2L    = &F2L{}
	f2D    = &F2D{}
	d2I    = &D2I{}
	d2L    = &D2L{}
	d2F    = &D2F{}
	i2B    = &I2B{}
	i2C    = &I2C{}
	i2S    = &I2S{}
	lCmp   = &LCmp{}
	fCmpL  = &FCmpL{}
	fCmpG  = &FCmpG{}
	dCmpL  = &DCmpL{}
	dCmpG  = &DCmpG{}
	// iReturn = &IReturn{}
	// lReturn = &LReturn{}
	// fReturn = &FReturn{}
	// dReturn = &DReturn{}
	// aReturn = &AReturn{}
	// Return = &Return{}
	// arrayLength = &ArrayLength{}
	// aThrow = &AThrow{}
	// monitOrEnter = &MonitOrEnter{}
	// monitOrExit = &MonitOrExit{}
	// invokeNative = &InvokeNative{}
)

func NewInstruction(operand byte) Instruction {
	switch operand {
	case 0x00:
		return nOp
	case 0x01:
		return aConstNull
	case 0x02:
		return iConstM1
	case 0x03:
		return iConst0
	case 0x04:
		return iConst1
	case 0x05:
		return iConst2
	case 0x06:
		return iConst3
	case 0x07:
		return iConst4
	case 0x08:
		return iConst5
	case 0x09:
		return lConst0
	case 0x0a:
		return lConst1
	case 0x0b:
		return fConst0
	case 0x0c:
		return fConst1
	case 0x0d:
		return fConst2
	case 0x0e:
		return dConst0
	case 0x0f:
		return dConst1
	case 0X10:
		return &BIPush{}
	case 0X11:
		return &SIPush{}
	// case 0X12:
	// return &LDC{}
	// case 0X13:
	// return &LDCW{}
	// case 0X14:
	// return &LDC2W{}
	case 0X15:
		return &ILoad{}
	case 0X16:
		return &LLoad{}
	case 0X17:
		return &FLoad{}
	case 0X18:
		return &DLoad{}
	case 0X19:
		return &ALoad{}
	case 0X1a:
		return iLoad0
	case 0X1b:
		return iLoad1
	case 0X1c:
		return iLoad2
	case 0X1d:
		return iLoad3
	case 0X1e:
		return lLoad0
	case 0X1f:
		return lLoad1
	case 0X20:
		return lLoad2
	case 0X21:
		return lLoad3
	case 0X22:
		return fLoad0
	case 0X23:
		return fLoad1
	case 0X24:
		return fLoad2
	case 0X25:
		return fLoad3
	case 0X26:
		return dLoad0
	case 0X27:
		return dLoad1
	case 0X28:
		return dLoad2
	case 0X29:
		return dLoad3
	case 0X2a:
		return aLoad0
	case 0X2b:
		return aLoad1
	case 0X2c:
		return aLoad2
	case 0X2d:
		return aLoad3
	// case 0X2e:
	// return iALoad
	// case 0X2f:
	// return lALoad
	// case 0x30:
	// return fALoad
	// case 0x31:
	// return dALoad
	// case 0x32:
	// return aALoad
	// case 0x33:
	// return bALoad
	// case 0x34:
	// return cALoad
	// case 0x35:
	// return sALoad
	case 0x36:
		return &IStore{}
	case 0x37:
		return &LStore{}
	case 0x38:
		return &FStore{}
	case 0x39:
		return &DStore{}
	case 0x3a:
		return &AStore{}
	case 0x3b:
		return iStore0
	case 0x3c:
		return iStore1
	case 0x3d:
		return iStore2
	case 0x3e:
		return iStore3
	case 0x3f:
		return lStore0
	case 0x40:
		return lStore1
	case 0x41:
		return lStore2
	case 0x42:
		return lStore3
	case 0x43:
		return fStore0
	case 0x44:
		return fStore1
	case 0x45:
		return fStore2
	case 0x46:
		return fStore3
	case 0x47:
		return dStore0
	case 0x48:
		return dStore1
	case 0x49:
		return dStore2
	case 0x4a:
		return dStore3
	case 0x4b:
		return aStore0
	case 0x4c:
		return aStore1
	case 0x4d:
		return aStore2
	case 0x4e:
		return aStore3
	// case 0x4f:
	// return iAStore
	// case 0x50:
	// return lAStore
	// case 0x51:
	// return fAStore
	// case 0x52:
	// return dAStore
	// case 0x53:
	// return aAStore
	// case 0x54:
	// return bAStore
	// case 0x55:
	// return cAStore
	// case 0x56:
	// return sAStore
	case 0x57:
		return pop
	case 0x58:
		return pop2
	case 0x59:
		return dup
	case 0x5a:
		return dupX1
	case 0x5b:
		return dupX2
	case 0x5c:
		return dup2
	case 0x5d:
		return dup2X1
	case 0x5e:
		return dup2X2
	case 0x5f:
		return swap
	case 0x60:
		return iAdd
	case 0x61:
		return lAdd
	case 0x62:
		return fAdd
	case 0x63:
		return dAdd
	case 0x64:
		return iSub
	case 0x65:
		return lSub
	case 0x66:
		return fSub
	case 0x67:
		return dSub
	case 0x68:
		return iMul
	case 0x69:
		return lMul
	case 0x6a:
		return fMul
	case 0x6b:
		return dMul
	case 0x6c:
		return iDiv
	case 0x6d:
		return lDiv
	case 0x6e:
		return fDiv
	case 0x6f:
		return dDiv
	case 0x70:
		return iRem
	case 0x71:
		return lRem
	case 0x72:
		return fRem
	case 0x73:
		return dRem
	case 0x74:
		return iNeg
	case 0x75:
		return lNeg
	case 0x76:
		return fNeg
	case 0x77:
		return dNeg
	case 0x78:
		return iShL
	case 0x79:
		return lShL
	case 0x7a:
		return iShR
	case 0x7b:
		return lShR
	case 0x7c:
		return iuShR
	case 0x7d:
		return luShR
	case 0x7e:
		return iAnd
	case 0x7f:
		return lAnd
	case 0x80:
		return iOr
	case 0x81:
		return lOr
	case 0x82:
		return iXOr
	case 0x83:
		return lXOr
	case 0x84:
		return &IInc{}
	case 0x85:
		return i2L
	case 0x86:
		return i2F
	case 0x87:
		return i2D
	case 0x88:
		return l2I
	case 0x89:
		return l2F
	case 0x8a:
		return l2D
	case 0x8b:
		return f2I
	case 0x8c:
		return f2L
	case 0x8d:
		return f2D
	case 0x8e:
		return d2I
	case 0x8f:
		return d2L
	case 0x90:
		return d2F
	case 0x91:
		return i2B
	case 0x92:
		return i2C
	case 0x93:
		return i2S
	case 0x94:
		return lCmp
	case 0x95:
		return fCmpL
	case 0x96:
		return fCmpG
	case 0x97:
		return dCmpL
	case 0x98:
		return dCmpG
	case 0x99:
		return &IfEq{}
	case 0x9a:
		return &IfNE{}
	case 0x9b:
		return &IfLT{}
	case 0x9c:
		return &IfGE{}
	case 0x9d:
		return &IfGT{}
	case 0x9e:
		return &IfLE{}
	case 0x9f:
		return &IfICmpEq{}
	case 0xa0:
		return &IfICmpNE{}
	case 0xa1:
		return &IfICmpLT{}
	case 0xa2:
		return &IfICmpGE{}
	case 0xa3:
		return &IfICmpGT{}
	case 0xa4:
		return &IfICmpLE{}
	case 0xa5:
		return &IfACmpEq{}
	case 0xa6:
		return &IfACmpNE{}
	case 0xa7:
		return &GoTo{}
	// case 0xa8:
	// return &JSR{}
	case 0xa9:
		return &Ret{}
	case 0xaa:
		return &TableSwitch{}
	case 0xab:
		return &LookupSwitch{}
	// case 0xac:
	// return iReturn
	// case 0xad:
	// return lReturn
	// case 0xae:
	// return fReturn
	// case 0xaf:
	// return dReturn
	// case 0xb0:
	// return aReturn
	// case 0xb1:
	// return Return
	// case 0xb2:
	// return &GetStatic{}
	// case 0xb3:
	// return &PutStatic{}
	// case 0xb4:
	// return &GetField{}
	// case 0xb5:
	// return &PutField{}
	// case 0xb6:
	// return &InvokeVirtual{}
	// case 0xb7:
	// return &InvokeSpecial{}
	// case 0xb8:
	// return &InvokeStatic{}
	// case 0xb9:
	// return &InvokeInterface{}
	// case 0xba:
	// return &InvokeDynamic{}
	// case 0xbb:
	// return &New{}
	// case 0xbc:
	// return &NewArray{}
	// case 0xbd:
	// return &ANewArray{}
	// case 0xbe:
	// return arrayLength
	// case 0xbf:
	// return aThrow
	// case 0xc0:
	// return &CheckCast{}
	// case 0xc1:
	// return &InstanceOf{}
	// case 0xc2:
	// return monitOrEnter
	// case 0xc3:
	// return monitOrExit
	case 0xc4:
		return &Wide{}
	// case 0xc5:
	// return &MultiANewArray{}
	case 0xc6:
		return &IfNull{}
	case 0xc7:
		return &IfNonNull{}
	case 0xc8:
		return &GoToW{}
	// case 0xc9:
	// return &JSRW{}
	// case 0xfe:
	// return invokeNative
	// case 0xff:
	// return &Bootstrap{}
	default:
		panic("Unsupported operand: 0x" + strconv.FormatInt(int64(operand), 16))
	}
}
