package push

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type BIPush struct{ value int8 }

func (bIPush *BIPush) FetchOperands(byteCodeReader *common.ByteCodeReader) {
	bIPush.value = byteCodeReader.ReadInt8()
}

func (bIPush *BIPush) Execute(frame *runtime.Frame) {
	i := int32(bIPush.value)
	frame.OperandStack().PushInt(i)
}

type SIPush struct{ value int16 }

func (sIPush *SIPush) FetchOperands(byteCodeReader *common.ByteCodeReader) {
	sIPush.value = byteCodeReader.ReadInt16()
}

func (sIPush *SIPush) Execute(frame *runtime.Frame) {
	i := int32(sIPush.value)
	frame.OperandStack().PushInt(i)
}
