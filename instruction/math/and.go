package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IAnd struct {
	common.NoOperandsInstruction
}

func (iAnd *IAnd) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	result := int2 & int1
	stack.PushInt(result)
}

type LAnd struct {
	common.NoOperandsInstruction
}

func (lAnd *LAnd) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopLong()
	long2 := stack.PopLong()
	result := long2 & long1
	stack.PushLong(result)
}
