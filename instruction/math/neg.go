package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DNeg struct {
	common.NoOperandsInstruction
}

func (dNeg *DNeg) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopDouble()
	stack.PushDouble(-value)
}

type FNeg struct {
	common.NoOperandsInstruction
}

func (fNeg *FNeg) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopFloat()
	stack.PushFloat(-value)
}

type INeg struct {
	common.NoOperandsInstruction
}

func (iNeg *INeg) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopInt()
	stack.PushInt(-value)
}

type LNeg struct {
	common.NoOperandsInstruction
}

func (lNeg *LNeg) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	value := stack.PopLong()
	stack.PushLong(-value)
}
