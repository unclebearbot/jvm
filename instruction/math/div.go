package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DDiv struct {
	common.NoOperandsInstruction
}

func (dDiv *DDiv) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopDouble()
	double2 := stack.PopDouble()
	result := double2 / double1
	stack.PushDouble(result)
}

type FDiv struct {
	common.NoOperandsInstruction
}

func (fDiv *FDiv) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	float1 := stack.PopFloat()
	float2 := stack.PopFloat()
	result := float2 / float1
	stack.PushFloat(result)
}

type IDiv struct {
	common.NoOperandsInstruction
}

func (iDiv *IDiv) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int1 == 0 {
		panic("java.lang.ArithmeticException: divide by zero")
	}
	result := int2 / int1
	stack.PushInt(result)
}

type LDiv struct {
	common.NoOperandsInstruction
}

func (lDiv *LDiv) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopLong()
	int2 := stack.PopLong()
	if int1 == 0 {
		panic("java.lang.ArithmeticException: divide by zero")
	}
	result := int2 / int1
	stack.PushLong(result)
}
