package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DSub struct{ common.NoOperandsInstruction }

func (dSub *DSub) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopDouble()
	double2 := stack.PopDouble()
	result := double2 - double1
	stack.PushDouble(result)
}

type FSub struct{ common.NoOperandsInstruction }

func (fSub *FSub) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	float1 := stack.PopFloat()
	float2 := stack.PopFloat()
	result := float2 - float1
	stack.PushFloat(result)
}

type ISub struct{ common.NoOperandsInstruction }

func (iSub *ISub) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	result := int2 - int1
	stack.PushInt(result)
}

type LSub struct{ common.NoOperandsInstruction }

func (lSub *LSub) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopLong()
	long2 := stack.PopLong()
	result := long2 - long1
	stack.PushLong(result)
}
