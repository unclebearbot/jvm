package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DAdd struct{ common.NoOperandsInstruction }

func (dAdd *DAdd) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopDouble()
	double2 := stack.PopDouble()
	result := double1 + double2
	stack.PushDouble(result)
}

type FAdd struct{ common.NoOperandsInstruction }

func (fAdd *FAdd) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	float1 := stack.PopFloat()
	float2 := stack.PopFloat()
	result := float1 + float2
	stack.PushFloat(result)
}

type IAdd struct{ common.NoOperandsInstruction }

func (iAdd *IAdd) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	result := int1 + int2
	stack.PushInt(result)
}

type LAdd struct{ common.NoOperandsInstruction }

func (lAdd *LAdd) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopLong()
	long2 := stack.PopLong()
	result := long1 + long2
	stack.PushLong(result)
}
