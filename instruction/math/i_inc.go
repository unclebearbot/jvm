package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IInc struct {
	Index uint
	Const int32
}

func (iInc *IInc) FetchOperands(byteCodeReader *common.ByteCodeReader) {
	iInc.Index = uint(byteCodeReader.ReadUint8())
	iInc.Const = int32(byteCodeReader.ReadInt8())
}

func (iInc *IInc) Execute(frame *runtime.Frame) {
	localVariableTable := frame.LocalVariableTable()
	value := localVariableTable.GetInt(iInc.Index)
	value += iInc.Const
	localVariableTable.SetInt(iInc.Index, value)
}
