package math

import (
	"math"

	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DRem struct {
	common.NoOperandsInstruction
}

func (dRem *DRem) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopDouble()
	double2 := stack.PopDouble()
	result := math.Mod(double2, double1)
	stack.PushDouble(result)
}

type FRem struct {
	common.NoOperandsInstruction
}

func (fRem *FRem) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	float1 := stack.PopFloat()
	float2 := stack.PopFloat()
	result := float32(math.Mod(float64(float2), float64(float1)))
	stack.PushFloat(result)
}

type IRem struct {
	common.NoOperandsInstruction
}

func (iRem *IRem) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	if int1 == 0 {
		panic("java.lang.ArithmeticException: divide by zero")
	}
	result := int2 % int1
	stack.PushInt(result)
}

type LRem struct {
	common.NoOperandsInstruction
}

func (lRem *LRem) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopLong()
	long2 := stack.PopLong()
	if long1 == 0 {
		panic("java.lang.ArithmeticException: divide by zero")
	} else {
		result := long2 % long1
		stack.PushLong(result)
	}
}
