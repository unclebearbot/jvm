package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IShL struct {
	common.NoOperandsInstruction
}

func (iShL *IShL) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	shift := uint32(int1) & 0x1f
	result := int2 << shift
	stack.PushInt(result)
}

type IShR struct {
	common.NoOperandsInstruction
}

func (iShR *IShR) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopLong()
	shift := uint32(int1) & 0x3f
	result := int2 >> shift
	stack.PushLong(result)
}

type IUShR struct {
	common.NoOperandsInstruction
}

func (iUShR *IUShR) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	shift := uint32(int1) & 0x1f
	result := int32(uint32(int2) >> shift)
	stack.PushInt(result)
}

type LShL struct {
	common.NoOperandsInstruction
}

func (lShL *LShL) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopInt()
	long2 := stack.PopLong()
	shift := uint32(long1) & 0x3f
	result := long2 << shift
	stack.PushLong(result)
}

type LShR struct {
	common.NoOperandsInstruction
}

func (lShR *LShR) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopInt()
	long2 := stack.PopLong()
	shift := uint32(long1) & 0x3f
	result := long2 >> shift
	stack.PushLong(result)
}

type LUShR struct {
	common.NoOperandsInstruction
}

func (lUShR *LUShR) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	long1 := stack.PopInt()
	long2 := stack.PopLong()
	shift := uint32(long1) & 0x3f
	result := int64(uint64(long2) >> shift)
	stack.PushLong(result)
}
