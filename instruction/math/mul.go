package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type DMul struct {
	common.NoOperandsInstruction
}

func (dMul *DMul) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopDouble()
	double2 := stack.PopDouble()
	result := double2 * double1
	stack.PushDouble(result)
}

type FMul struct {
	common.NoOperandsInstruction
}

func (fMul *FMul) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopFloat()
	double2 := stack.PopFloat()
	result := double2 * double1
	stack.PushFloat(result)
}

type IMul struct {
	common.NoOperandsInstruction
}

func (iMul *IMul) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopInt()
	double2 := stack.PopInt()
	result := double2 * double1
	stack.PushInt(result)
}

type LMul struct {
	common.NoOperandsInstruction
}

func (lMul *LMul) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	double1 := stack.PopLong()
	double2 := stack.PopLong()
	result := double2 * double1
	stack.PushLong(result)
}
