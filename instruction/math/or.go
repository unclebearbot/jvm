package math

import (
	"bitbucket.org/nvxarm/jvm/instruction/common"
	"bitbucket.org/nvxarm/jvm/runtime"
)

type IOr struct {
	common.NoOperandsInstruction
}

func (iOr *IOr) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	result := int2 | int1
	stack.PushInt(result)
}

type IXOr struct {
	common.NoOperandsInstruction
}

func (iXOr *IXOr) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopInt()
	int2 := stack.PopInt()
	result := int2 ^ int1
	stack.PushInt(result)
}

type LOr struct {
	common.NoOperandsInstruction
}

func (lOr *LOr) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopLong()
	int2 := stack.PopLong()
	result := int2 | int1
	stack.PushLong(result)
}

type LXOr struct {
	common.NoOperandsInstruction
}

func (lXOr *LXOr) Execute(frame *runtime.Frame) {
	stack := frame.OperandStack()
	int1 := stack.PopLong()
	int2 := stack.PopLong()
	result := int2 ^ int1
	stack.PushLong(result)
}
